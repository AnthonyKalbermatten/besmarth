const mockedStore = {};
export async function deleteItemAsync(key){
  delete mockedStore[key];
  return Promise.resolve();
}

export async function setItemAsync(key, value){
  mockedStore[key] = value;
  return Promise.resolve();
}

export async function getItemAsync(key){
  return Promise.resolve(mockedStore[key] || null);
}
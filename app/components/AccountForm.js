import React from "react";
import {Dimensions, ScrollView, StyleSheet, View, Alert} from "react-native";
import {Picker} from '@react-native-picker/picker';
import {bindActionCreators} from "redux";
import {deleteAccount, updateAccount} from "../services/AccountService";
import {initAuth, signOut} from "../services/Auth";
import {connect} from "react-redux";
import Navigation from "../services/Navigation";
import {Text, Button, Dialog, Paragraph, Portal, Switch, TextInput} from "react-native-paper";
import ErrorBox from "./ErrorBox";
import {emptyString} from "../services/utils";
import {showToast} from "../services/Toast";

/**
 * Form which enables the user to edit his account details
 */
class AccountForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            acceptDataUsage: false,
            pseudonymous: false,
            gender: "---",
            age: "---"
        };
    }

    componentDidMount() {
        if (!this.props.signUpMode) {
            this.setState({...this.state, ...this.props.account});
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.account.username !== prevProps.account.username) {
            if (!this.props.signUpMode) {
                this.setState({...this.props.account});
            }
        }
    }

    async onSubmit() {
        if (this.props.signUpMode && !this.state.acceptDataUsage) {
            Alert.alert("Hinweis", "Du musst der Datenschutzerklärung zustimmen");
        } else if (this.props.signUpMode && emptyString(this.state.password)) {
            Alert.alert("Hinweis", "Bitte gib ein Passwort ein");
        } else if (this.props.signUpMode && emptyString(this.state.username)) {
            Alert.alert("Hinweis", "Bitte gib ein Benutzername ein");
        }else {
            try {
                await this.props.updateAccount(this.state);
                if (this.props.signUpMode) {
                    this.props.showToast("Account erfolgreich angelegt");
                } else {
                    this.props.showToast("Daten erfolgreich gespeichert");
                }
                this.setState({...this.state, errors: null});
                Navigation.navigate("Initial");
            } catch (e) {
                this.setState({
                    ...this.state,
                    errors: e
                });
            }
        }
    }

    onChangeField(fieldName, newValue) {
        this.setState({
            ...this.state,
            [fieldName]: newValue
        });
    }

    async switchAccount() {
        this.setState({...this.state, pseudonymous: true})
        await this.props.updateAccount(this.state);
        await signOut(this);
        Navigation.navigate("LoginStack");
    }

    /**
     * deletes account and then creates anonymous account
     */
    async deleteAccount() {
        try {
            await this.props.deleteAccount(this.state);
            await signOut(this);
            await this.props.initAuth();
            this.props.showToast("Account erfolgreich gelöscht");
        } catch (e) {
            this.props.showToast("Account konnte nicht gelöscht werden, versuche es später noch einmal", "ERROR");
        }
    }

    _showDeleteDialog = () => this.setState({deleteDialogVisible: true});
    _hideDeleteDialog = () => this.setState({deleteDialogVisible: false});

    render() {
        let acceptDataUsage;
        let loginWithAnotherAccount;
        let deleteAccountButton;

        if (this.props.signUpMode) {
            acceptDataUsage = <View style={styles.row}>
                <Switch value={this.state.acceptDataUsage}
                        onValueChange={val => this.onChangeField("acceptDataUsage", val)}/>
                <Paragraph>
                    <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL("http://86.119.43.87/privacy-policy/")}> Datenschutzerklärung </Text>
                    akzeptieren
                    <Text style={styles.mandatory}>*</Text>
                </Paragraph>
            </View>;
        } else {
            /*loginWithAnotherAccount =
                <Button style={styles.button} mode="outlined" accessibilityLabel="Account wechseln" disabled={true}
                        onPress={this.switchAccount.bind(this)}>Logout</Button>;*/
            deleteAccountButton =
                <Button style={styles.button} accessibilityLabel="Account löschen" mode="outlined" color={"red"}
                        onPress={this._showDeleteDialog}>Account löschen</Button>;
        }

        return (<ScrollView keyboardShouldPersistTaps="handled" style={styles.container}>
            <ErrorBox errors={this.state.errors}></ErrorBox>

            <View style={styles.inputRow}>
                <TextInput
                    style={styles.inputRowElement}
                    label="Vorname"
                    value={this.state.first_name}
                    accessibilityLabel="Vorname"
                    onChangeText={val => this.onChangeField("first_name", val)}
                />
                <TextInput
                    style={styles.inputRowElement}
                    label="E-Mail"
                    keyboardType="email-address"
                    autoCapitalize="none"
                    accessibilityLabel="E-Mail"
                    value={this.state.email}
                    onChangeText={val => this.onChangeField("email", val)}
                />
            </View>
            <View style={styles.inputRow}>
                <TextInput
                    style={styles.inputRowElement}
                    label="Benutzername*"
                    autoCapitalize="none"
                    accessibilityLabel="Benutzername"
                    value={this.state.username}
                    onChangeText={val => this.onChangeField("username", val)}
                />
                <TextInput
                    style={styles.inputRowElement}
                    label="Neues Passwort*"
                    secureTextEntry={true}
                    autoCapitalize="none"
                    accessibilityLabel="Neues Passwort"
                    value={this.state.password}
                    onChangeText={val => this.onChangeField("password", val)}
                />
            </View>
            <View style={styles.inputRow}>
                <View style={styles.inputRowElementPicker}>
                    <Text style={styles.inputRowElementPickerTitle}>Alter</Text>
                    <Picker
                        style={{height: 44}}
                        itemStyle={{height: 44}}
                        selectedValue={this.state.age}
                        onValueChange={val => this.onChangeField("age", val)}>
                        <Picker.Item label="---" value="---"/>
                        <Picker.Item label="0 bis 25" value="0 bis 25"/>
                        <Picker.Item label="26 bis 50" value="26 bis 50"/>
                        <Picker.Item label="51 bis 75" value="51 bis 75"/>
                        <Picker.Item label="76 bis 100" value="76 bis 100"/>
                    </Picker>
                </View>
                <View style={styles.inputRowElementPicker}>
                    <Text style={styles.inputRowElementPickerTitle}>Geschlecht</Text>
                    <Picker
                        style={{height: 44}}
                        itemStyle={{height: 44}}
                        selectedValue={this.state.gender}
                        onValueChange={val => this.onChangeField("gender", val)}>
                        <Picker.Item label="---" value="---"/>
                        <Picker.Item label="männlich" value="männlich"/>
                        <Picker.Item label="weiblich" value="weiblich"/>
                        <Picker.Item label="anderes" value="anderes"/>
                    </Picker>
                </View>
            </View>

            {acceptDataUsage}
            <Button style={styles.button} mode="contained" accessibilityLabel={this.props.submitTitle}
                    onPress={this.onSubmit.bind(this)}>{this.props.submitTitle}</Button>

            {loginWithAnotherAccount}
            {deleteAccountButton}

            <Portal>
                <Dialog
                    visible={this.state.deleteDialogVisible}
                    onDismiss={this._hideDeleteDialog}>
                    <Dialog.Title>Account löschen?</Dialog.Title>
                    <Dialog.Content>
                        <Paragraph>Deine persönlichen Daten auf unserem Server werden gelöscht.</Paragraph>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button onPress={this._hideDeleteDialog}>Nein</Button>
                        <Button accessibilityLabel="Account löschen bestätigen" onPress={async () => {
                            this.deleteAccount(this).then();
                            this._hideDeleteDialog();
                        }}>Ja</Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>
        </ScrollView>);
    }
}

const {width, height} = Dimensions.get("window");
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10
    },
    row: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center"
    },
    button: {
        marginVertical: 8
    },
    inputRow: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    inputRowElement: {
        width: '47%'
    },
    inputRowElementPicker: {
        width: '47%',
        marginTop: 10
    },
    inputRowElementPickerTitle: {
        paddingLeft: 10,
        fontSize: 12,
        color: 'gray'
    },
    mandatory: {
        color: 'red'
    }
});

const mapStateToProps = state => {
    return {account: state.account.account};
};

const mapDispatchToProps = dispatch => bindActionCreators({
    updateAccount,
    initAuth,
    deleteAccount,
    showToast
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AccountForm);

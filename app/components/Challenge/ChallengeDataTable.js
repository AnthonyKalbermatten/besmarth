import {Divider} from "react-native-paper";
import {StyleSheet, View, Text} from "react-native";
import React from "react";

/**
 * Displays difficulty and duration about a challenge in a data table.
 * Is not connected with the redux store
 * Use like <ChallengeDataTable challenge={yourChallenge} />
 */
export class ChallengeDataTable extends React.Component {

    periodicityToString(periodicity) {
        switch (periodicity.toLowerCase()) {
            case "daily":
                return "Täglich";
            case "weekly":
                return "Wöchentlich";
            case "monthly":
                return "Monatlich";
            default:
                return "<Unknown periodicity " + periodicity;
        }
    }

    periodicityToUnitString(periodicity) {
        switch (periodicity.toLowerCase()) {
            case "daily":
                return "Tage";
            case "weekly":
                return "Wochen";
            case "monthly":
                return "Monate";
            default:
                return "<Unknown periodicity " + periodicity;
        }
    }

    render() {
        const challenge = this.props.challenge;

        return (
            <View>
                <View style={styles.rowView}>
                    <View style={styles.innerRow}>
                        <Text style={styles.title}>Frequenz: </Text>
                        <Text>{this.periodicityToString(challenge.periodicity)}</Text>
                    </View>
                    <View style={styles.innerRow}>
                        <Text style={styles.title}>Dauer: </Text>
                        <Text>{challenge.duration} {this.periodicityToUnitString(challenge.periodicity)}</Text>
                    </View>
                </View>
                <Divider/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    rowView: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    innerRow: {
        flex: 1,
        flexDirection: 'row',
        padding: 10
    },
    title: {
        fontWeight: 'bold'
    }
});
import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';import {getFocusedRouteNameFromRoute} from '@react-navigation/native';
import HomeScreen from "../screens/HomeScreen";
import DailyTipScreen from "../screens/DailyTipScreen";
import TopicScreen from "../screens/TopicScreen";
import ChallengesScreen from "../screens/ChallengesScreen";
import ChallengeDetailScreen from "../screens/ChallengeDetailScreen";
import MyChallengesScreen from "../screens/MyChallengesScreen";
import FriendsScreen from "../screens/Friends/FriendsScreen";
import FriendDetailScreen from "../screens/FriendDetailScreen";
import ManageAccountScreen from "../screens/Account/ManageAccountScreen";
import SignInScreen from "../screens/Account/SignInScreen";
import SignUpScreen from "../screens/Account/SignUpScreen";
import AccountCodeScreen from "../screens/Friends/AccountCodeScreen";
import ScanFriendCodeScreen from "../screens/Friends/ScanFriendCodeScreen";
import RewardScreen from "../screens/RewardScreen";
import InfotrailerScreen from "../screens/InfotrailerScreen";
import ChallengeReasonScreen from "../screens/ChallengeReasonScreen";
import ChallengeHypothesisScreen from "../screens/ChallengeHypothesisScreen";
import ChallengePushSettingScreen from "../screens/ChallengePushSettingScreen";
import DailyObservationScreen from "../screens/Diary/DailyMonitoring/DailyObservationScreen";
import DailyReflectionScreen from "../screens/Diary/DailyMonitoring/DailyReflectionScreen";
import WeeklyExperienceReportScreen from "../screens/Diary/WeeklyMonitoring/WeeklyExperienceReportScreen";
import WeeklyFactorsOverviewScreen from "../screens/Diary/WeeklyMonitoring/WeeklyFactorsOverviewScreen";
import WeeklyActionsScreen from "../screens/Diary/WeeklyMonitoring/WeeklyActionsScreen";
import ExperienceOverviewScreen from "../screens/Diary/ExperienceOverviewScreen";
import FinalReportIntroScreen from "../screens/Diary/FinalMonitoring/FinalReportIntroScreen";
import FinalReportScreen from "../screens/Diary/FinalMonitoring/FinalReportScreen";
import FinalReportFactorsScreen from "../screens/Diary/FinalMonitoring/FinalReportFactorsScreen";
import FinalReportSurveyScreen from "../screens/Diary/FinalMonitoring/FinalReportSurveyScreen";
import AddChallengeScreen from "../screens/AddChallengeScreen";
import WeeklyMonitoringOverviewScreen from "../screens/Diary/WeeklyMonitoring/WeeklyMonitoringOverviewScreen"
import ViewWeeklyExperienceReportScreen from "../screens/Diary/WeeklyMonitoring/ViewWeeklyExperienceReportScreen";
import ViewWeeklyFactorsOverviewScreen from "../screens/Diary/WeeklyMonitoring/ViewWeeklyFactorsOverviewScreen";
import ViewWeeklyActionsScreen from "../screens/Diary/WeeklyMonitoring/ViewWeeklyActionsScreen";
import ViewDailyObservationScreen from "../screens/Diary/DailyMonitoring/ViewDailyObservationScreen";
import ViewDailyReflectionScreen from "../screens/Diary/DailyMonitoring/ViewDailyReflectionScreen";
import ViewFinalReportIntroScreen from "../screens/Diary/FinalMonitoring/ViewFinalReportIntroScreen";
import ViewFinalReportScreen from "../screens/Diary/FinalMonitoring/ViewFinalReportScreen";
import ViewFinalReportFactorsScreen from "../screens/Diary/FinalMonitoring/ViewFinalReportFactorsScreen";
import UpdateDailyObservationScreen from "../screens/Diary/DailyMonitoring/UpdateDailyObservationScreen";
import UpdateDailyReflectionScreen from "../screens/Diary/DailyMonitoring/UpdateDailyReflectionScreen";
import MonitoringCorrectionScreen from "../screens/Diary/MonitoringCorrectionScreen";
import MonitoringEnterUpScreen from "../screens/Diary/MonitoringEnterUpScreen";
import SharingOverviewScreen from "../screens/Sharing/SharingOverviewScreen";
import GroupMembersScreen from "../screens/Sharing/GroupMembersScreen";
import CalendarOverviewScreen from "../screens/CalendarOverviewScreen"
import {Colors} from "../styles/index";
import TabBarIcon from "../components/TabBarIcon";

const HomeStack = createStackNavigator();

export function HomeStackScreen() {
    return (
        <HomeStack.Navigator initialRouteName="Infotrailer">
            <HomeStack.Screen name="Home" component={HomeScreen}
                              options={{
                                  title: "Wahl des Self-Commitments",
                                  headerStyle: {backgroundColor: Colors.green},
                                  headerTintColor: Colors.white,
                                  headerLeft: null,
                              }}/>
            <HomeStack.Screen name="Challenges" component={ChallengesScreen}
                              options={{
                                  title: "Wahl des Self-Commitments",
                                  headerStyle: {backgroundColor: Colors.green},
                                  headerTintColor: Colors.white
                              }}/>
            <HomeStack.Screen name="ChallengeDetail" component={ChallengeDetailScreen}
                              options={{
                                  title: "Wahl des Self-Commitments",
                                  headerStyle: {backgroundColor: Colors.green},
                                  headerTintColor: Colors.white
                              }}/>
            <HomeStack.Screen name="ChallengeReason" component={ChallengeReasonScreen}
                              options={{
                                  title: "Wahl des Self-Commitments",
                                  headerStyle: {backgroundColor: Colors.green},
                                  headerTintColor: Colors.white
                              }}/>
            <HomeStack.Screen name="ChallengeHypothesis" component={ChallengeHypothesisScreen}
                              options={{
                                  title: "Wahl des Self-Commitments",
                                  headerStyle: {backgroundColor: Colors.green},
                                  headerTintColor: Colors.white
                              }}/>
            <HomeStack.Screen name="ChallengePushSetting" component={ChallengePushSettingScreen}
                              options={{
                                  title: "Wahl des Self-Commitments",
                                  headerStyle: {backgroundColor: Colors.green},
                                  headerTintColor: Colors.white
                              }}/>
        </HomeStack.Navigator>
    );
}

const MyChallengesStack = createStackNavigator();

export function MyChallengesStackScreen() {
    return (
        <MyChallengesStack.Navigator initialRouteName="MyChallenges">
            <MyChallengesStack.Screen name="MyChallenges" component={MyChallengesScreen}
                                      options={{
                                          title: "Meine Commitments",
                                          headerStyle: {backgroundColor: Colors.green},
                                          headerTintColor: Colors.white,
                                          headerLeft: null,
                                      }}/>
            <MyChallengesStack.Screen name="ChallengeDetail" component={ChallengeDetailScreen}
                                      options={{
                                          title: "Commitment",
                                          headerStyle: {backgroundColor: Colors.green},
                                          headerTintColor: Colors.white
                                      }}/>
        </MyChallengesStack.Navigator>
    );
}

const SharingStack = createStackNavigator();

export function SharingStackScreen() {
    return (
        <SharingStack.Navigator initialRouteName="SharingOverview">
            <SharingStack.Screen name="SharingOverview" component={SharingOverviewScreen}
            options={{
                title: "Sharing",
                headerStyle: {backgroundColor: Colors.green},
                headerTintColor: Colors.white,
                headerLeft: null,
            }}/>
            <SharingStack.Screen name="GroupMembers" component={GroupMembersScreen}
            options={{
                title: "Gruppenmitglieder",
                headerStyle: {backgroundColor: Colors.green},
                headerTintColor: Colors.white,
                headerLeft: null,
            }}/>
        </SharingStack.Navigator>
    );
}

//not in use
const FriendsStack = createStackNavigator();

export function FriendsStackScreen() {
    return (
        <FriendsStack.Navigator initialRouteName="Friends">
            <FriendsStack.Screen name="Friends" component={FriendsScreen}
                                 options={{
                                     title: "Freunde",
                                     headerStyle: {backgroundColor: Colors.green},
                                     headerTintColor: Colors.white,
                                     headerLeft: null,
                                 }}/>
            <FriendsStack.Screen name="FriendDetail" component={FriendDetailScreen}
                                 options={{
                                     title: "Freund",
                                     headerStyle: {backgroundColor: Colors.green},
                                     headerTintColor: Colors.white
                                 }}/>
            <FriendsStack.Screen name="AccountCode" component={AccountCodeScreen}
                                 options={{
                                     title: "Dein Code",
                                     headerStyle: {backgroundColor: Colors.green},
                                     headerTintColor: Colors.white
                                 }}/>
            <FriendsStack.Screen name="ScanFriendCode" component={ScanFriendCodeScreen}
                                 options={{
                                     title: "Code scannen",
                                     headerStyle: {backgroundColor: Colors.green},
                                     headerTintColor: Colors.white
                                 }}/>
            <FriendsStack.Screen name="ChallengeDetail" component={ChallengeDetailScreen}
                                 options={{
                                     title: "Challenge",
                                     headerStyle: {backgroundColor: Colors.green},
                                     headerTintColor: Colors.white
                                 }}/>
        </FriendsStack.Navigator>
    );
}

const AccountStack = createStackNavigator();

export function AccountStackScreen() {
    return (
        <AccountStack.Navigator initialRouteName="ManageAccount">
            <AccountStack.Screen name="ManageAccount" component={ManageAccountScreen}
                                 options={{
                                     title: "Accountverwaltung",
                                     headerStyle: {backgroundColor: Colors.green},
                                     headerTintColor: Colors.white,
                                     headerLeft: null,
                                 }}/>
            <AccountStack.Screen name="Infotrailer" component={InfotrailerScreen} options={{
                title: "BNE-App",
                headerStyle: {backgroundColor: Colors.green},
                headerTintColor: Colors.white
            }}/>
            <AccountStack.Screen name="Rewards" component={RewardScreen}
                                 options={{
                                     title: "Belohnungen",
                                     headerStyle: {backgroundColor: Colors.green},
                                     headerTintColor: Colors.white
                                 }}/>
        </AccountStack.Navigator>
    );
}

const DiaryStack = createStackNavigator();

export function DiaryStackScreen() {
    return (
        <DiaryStack.Navigator initialRouteName="ExperienceOverview">
            <DiaryStack.Screen name="ExperienceOverview" component={ExperienceOverviewScreen}
                               options={{
                                   title: "Meine Tagebücher",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                                   headerLeft: null,
                               }}/>
            <DiaryStack.Screen name="DailyObservation" component={DailyObservationScreen}
                               options={{
                                   title: "Mein Self-Commitment heute",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                                   headerLeft: null,
                               }}/>
            <DiaryStack.Screen name="ViewDailyObservation" component={ViewDailyObservationScreen}
                               options={{
                                   title: "Mein Self-Commitment heute",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                               }}/>
            <DiaryStack.Screen name="UpdateDailyObservation" component={UpdateDailyObservationScreen}
                               options={{
                                   title: "Mein Self-Commitment ändern",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                               }}/>
            <DiaryStack.Screen name="DailyReflection" component={DailyReflectionScreen}
                               options={{
                                   title: "Mein SelfCommitment heute",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                                   headerLeft: null,
                               }}/>
            <DiaryStack.Screen name="ViewDailyReflection" component={ViewDailyReflectionScreen}
                               options={{
                                   title: "Mein Self-Commitment heute",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                               }}/>
            <DiaryStack.Screen name="UpdateDailyReflection" component={UpdateDailyReflectionScreen}
                               options={{
                                   title: "Mein SelfCommitment ändern",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                               }}/>
            <DiaryStack.Screen name="WeeklyOverview" component={WeeklyMonitoringOverviewScreen}
                               options={{
                                   title: "Wochenrückblicke",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                                   headerLeft: null,
                               }}/>
            <DiaryStack.Screen name="WeeklyExperienceReport" component={WeeklyExperienceReportScreen}
                               options={{
                                   title: "Wochenrückblick",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                                   headerLeft: null,
                               }}/>
            <DiaryStack.Screen name="ViewWeeklyExperienceReport" component={ViewWeeklyExperienceReportScreen}
                               options={{
                                   title: "Wochenrückblick",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                               }}/>
            <DiaryStack.Screen name="WeeklyFactorsOverview" component={WeeklyFactorsOverviewScreen}
                               options={{
                                   title: "Wochenrückblick",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                                   headerLeft: null,
                               }}/>
            <DiaryStack.Screen name="ViewWeeklyFactorsOverview" component={ViewWeeklyFactorsOverviewScreen}
                               options={{
                                   title: "Wochenrückblick",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                               }}/>
            <DiaryStack.Screen name="WeeklyActions" component={WeeklyActionsScreen}
                               options={{
                                   title: "Wochenrückblick",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                                   headerLeft: null,
                               }}/>
            <DiaryStack.Screen name="ViewWeeklyActions" component={ViewWeeklyActionsScreen}
                               options={{
                                   title: "Wochenrückblick",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                               }}/>
            <DiaryStack.Screen name="FinalReportIntro" component={FinalReportIntroScreen}
                               options={{
                                   title: "Rückblick Self-Commitment",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                                   headerLeft: null,
                               }}/>
            <DiaryStack.Screen name="ViewFinalReportIntro" component={ViewFinalReportIntroScreen}
                               options={{
                                   title: "Rückblick Self-Commitment",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                               }}/>
            <DiaryStack.Screen name="FinalReport" component={FinalReportScreen}
                               options={{
                                   title: "Rückblick Self-Commitment",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                                   headerLeft: null,
                               }}/>
            <DiaryStack.Screen name="ViewFinalReport" component={ViewFinalReportScreen}
                               options={{
                                   title: "Rückblick Self-Commitment",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                               }}/>
            <DiaryStack.Screen name="FinalReportFactors" component={FinalReportFactorsScreen}
                               options={{
                                   title: "Rückblick Self-Commitment",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                                   headerLeft: null,
                               }}/>
            <DiaryStack.Screen name="ViewFinalReportFactors" component={ViewFinalReportFactorsScreen}
                               options={{
                                   title: "Rückblick Self-Commitment",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                               }}/>
            <DiaryStack.Screen name="FinalReportSurvey" component={FinalReportSurveyScreen}
                               options={{
                                   title: "Rückblick Self-Commitment",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                               }}/>
            <DiaryStack.Screen name="CorrectMonitoring" component={MonitoringCorrectionScreen}
                               options={{
                                   title: "Monitoring Korrektur",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                               }}/>
             <DiaryStack.Screen name="EnterUpMonitoring" component={MonitoringEnterUpScreen}
                               options={{
                                   title: "Nachtragen Monitoring",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                               }}/>
            <DiaryStack.Screen name="CalendarScreen" component={CalendarOverviewScreen}
                               options={{
                                   title: "Kalender",
                                   headerStyle: {backgroundColor: Colors.green},
                                   headerTintColor: Colors.white,
                               }}/>
        </DiaryStack.Navigator>
    );
}

const LoginStack = createStackNavigator();

export function LoginStackScreen() {
    return (
        <LoginStack.Navigator initialRouteName="Infotrailer">
            <LoginStack.Screen name="Infotrailer" component={InfotrailerScreen} options={{
                title: "BNE-App",
                headerStyle: {backgroundColor: Colors.green},
                headerTintColor: Colors.white,
                headerLeft: null,
            }}/>
            <LoginStack.Screen name="SignIn" component={SignInScreen} options={{
                title: "Einloggen",
                headerStyle: {backgroundColor: Colors.green},
                headerTintColor: Colors.white,
                headerLeft: null,
            }}/>
            <LoginStack.Screen name="SignUp" component={SignUpScreen} options={{
                title: "Registrieren",
                headerStyle: {backgroundColor: Colors.green},
                headerTintColor: Colors.white
            }}/>
        </LoginStack.Navigator>
    );
}

const Tab = createBottomTabNavigator();

export function TabNavigator() {
    const screenOptions = (label, iconIos, iconAndroid) => {
        const icon = Platform.OS === "ios" ? iconIos : iconAndroid;
        return ({route}) => ({
            tabBarLabel: label,
            tabBarIcon: ({focused, color, size}) => <TabBarIcon
                name={icon}
                focused={focused}/>
        });
    };
    return (
        <Tab.Navigator
            screenOptions={{ headerShown: false }}
            backBehaviour="history"
            shifting={false}
            labeled={true}
            barStyle={{backgroundColor: "white"}}>
            <Tab.Screen name="HomeTab" component={HomeStackScreen}
                        options={screenOptions("Home", "ios-home", "md-home")}/>
            <Tab.Screen name="DiaryTab" component={DiaryStackScreen}
                        options={screenOptions("Tagebuch", "ios-bookmark", "md-bookmark")}/>
            <Tab.Screen name="MyChallengesTab" component={MyChallengesStackScreen}
                        options={screenOptions("Commitments", "ios-speedometer", "md-speedometer")}/>
            <Tab.Screen name="SharingTab" component={SharingStackScreen}
                        options={screenOptions("Sharing", "ios-people", "md-people")}/>
            <Tab.Screen name="AccountTab" component={AccountStackScreen}
                        options={screenOptions("Account", "ios-person", "md-person")}/>
        </Tab.Navigator>
    );
}

const InitialStack = createStackNavigator();

export function InitialStackScreen() {
    return (
        <InitialStack.Navigator initialRouteName="InfotrailerSignedIn">
            <InitialStack.Screen name="InfotrailerSignedIn" component={InfotrailerScreen} options={{
                title: "BNE-App",
                headerStyle: {backgroundColor: Colors.green},
                headerTintColor: Colors.white
            }}/>
            <InitialStack.Screen name="Initial" component={TabNavigator} options={{
                headerShown: false
            }}/>
            <InitialStack.Screen name="LoginStack" component={LoginStackScreen} options={{
                headerShown: false
            }}/>
        </InitialStack.Navigator>
    );
}
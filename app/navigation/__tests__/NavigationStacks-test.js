import {renderForSnapshot} from "../../test/test-utils";
import {NavigationContainer} from "@react-navigation/native";
import {AccountStackScreen, FriendsStackScreen, HomeStackScreen, MyChallengesStackScreen} from "../NavigationStacks";
import React from "react";


describe("NavigationStacks", () => {
  it(`test HomeStackScreen`, () => {
    const tree = renderForSnapshot(<NavigationContainer><HomeStackScreen/></NavigationContainer>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it(`test AccountStackScreen`, () => {
    const tree = renderForSnapshot(<NavigationContainer><AccountStackScreen/></NavigationContainer>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it(`test FriendsStackScreen`, () => {
    const tree = renderForSnapshot(<NavigationContainer><FriendsStackScreen/></NavigationContainer>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it(`test MyChallengesStackScreen`, () => {
    const tree = renderForSnapshot(<NavigationContainer><MyChallengesStackScreen/></NavigationContainer>).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
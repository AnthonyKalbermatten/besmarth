export const initialReduxState = {
  app: {
    loading: true,
    error: null,
    toastVisible: false,
    toastMessage: null,
    rewardVisible: false,
    rewardMessage: null,
    rewards: null
  }
};

export default function account(state = initialReduxState.app, action) {
  switch (action.type) {
    case "APP_LOADING_COMPLETE":
      console.log("App loading complete!");
      return {
        ...state,
        loading: false,
      };
    case "APP_ERROR": {
      return {
        ...state,
        loading: state.loading,
        error: action.error
      };
    }
    case "SHOW_TOAST": {
      return {
        ...state,
        toastVisible: true,
        toastMessage: action.message,
        toastType: action.toastType
      };
    }
    case "HIDE_TOAST": {
      return {
        ...state,
        toastVisible: false,
        toastMessage: null
      };
    }
    case 'SHOW_REWARDS': {
      return {
        ...state,
        rewardsVisible: true,
        rewards: action.rewards
      }
    }
    case 'HIDE_REWARDS': {
      return {
        ...state,
        rewardsVisible: false,
        rewards: undefined
      }
    }
    default:
      return state;
  }
}
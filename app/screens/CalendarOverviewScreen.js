import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, View, Text, Button} from 'react-native';
import { FAB, Paragraph } from "react-native-paper";
import {Colors} from "../styles";


//not in use but could serve as basis to eventually replace ExperienceOverviewScreen
//calendar basis from https://code.tutsplus.com/tutorials/how-to-create-a-react-native-calendar-component--cms-33664



class CalendarOverviewScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedDate: new Date()
        };
    }

    weekDaysAbbrevationList = ["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"];
    monthsList = ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"]
    daysPerMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    startOfCommitment = this.props?.route?.params.diary.daily_monitorings;


     createCalenderMatrix() {
        var calendarMatrix = [];
        // first entry is all days of the week
        calendarMatrix[0] = this.weekDaysAbbrevationList;
        // set current month and year
        var year = this.state.selectedDate.getFullYear();
        var month = this.state.selectedDate.getMonth();
        //set first day of month
        var firstDay = new Date(year, month, 1).getDay();
        //get amount of days of month
        var maxDays = this.daysPerMonth[month];
            if (month == 1) { // February to handle leap year cases by adding one more day to 28
                if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
                    maxDays += 1;
                }
            }

        //set counter to 1 for first day of month
        var counter = 1;
        //go through every row and add array for each arrow, filled with
        for (var row = 1; row < 7; row++) {
          calendarMatrix[row] = [];
          for (var col = 0; col < 7; col++) {
            //initialize all values
            calendarMatrix[row][col] = -1;
            // Fill in rows only after the first day of the month whose position of list index
            if (row == 1 && col >= firstDay - 1) {
              calendarMatrix[row][col] = counter++;
            } else if (row > 1 && counter <= maxDays) {
              // Fill in remaining days of the month, capped by length of month
              calendarMatrix[row][col] = counter++;
            }
          }
        }
        return calendarMatrix;
    }

    _onPress = (item) => {
    this.setState(() => {
      if (!item.match && item != -1) {
        this.state.selectedDate.setDate(item);
        return this.state;
        }
      });
      //Navigation.navigate("DiaryTab", "UpdateDailyReflection", {diary: this.props?.route?.params.diary}
    };

    changeMonth = (n) => {
    this.setState(() => {
      this.state.selectedDate.setMonth(
        this.state.selectedDate.getMonth() + n
      )
      return this.state;
    });
}


    render() {

       matrix = this.createCalenderMatrix();
       console.log(this.monthsList[this.state.selectedDate.getMonth()])
       console.log(this.state.selectedDate.getFullYear())
       console.log(this.props?.route?.params.diary)
       console.log(this.state.selectedDate)
       console.log(this.startOfCommitment)

       var rows = [];
        rows = matrix.map((row, rowIndex) => {
          var rowItems = row.map((item, colIndex) => {
            return (
              <Text
                style={{
                  flex: 1,
                  height: 18,
                  textAlign: 'center',
                  // Highlight header
                  backgroundColor: rowIndex == 0 ? '#ddd' : '#fff',
                  // Highlight weekend
                  color: colIndex == 5 || colIndex == 6? '#a00' : '#000',
                  // Highlight current date
                  fontWeight: item == this.state.selectedDate.getDate()
                                      ? 'bold': 'normal'
                }}
                onPress={() => this._onPress(item)}>
                {item != -1 ? item : ''}
              </Text>
            );
          });


          return (
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                padding: 15,
                justifyContent: 'space-around',
                alignItems: 'center',
              }}>
              {rowItems}
            </View>
          );
        });

        //actual rendering
        return (
            <View>
            <Text style={{
              fontWeight: 'bold',
              fontSize: 18,
              textAlign: 'center'
            }}>
            {this.monthsList[this.state.selectedDate.getMonth()]} &nbsp;
            {this.state.selectedDate.getFullYear()}
            </Text>
            { rows }
            <FAB style={styles.button}  label="Vorheriger Monat" accessibilityLabel="Vorheriger Monat" onPress={() => this.changeMonth(-1)}/>
            <FAB style={styles.button}  label="Nächster Monat" accessibilityLabel="Nächster Monat"     onPress={() => this.changeMonth(+1)}/>
            </View>
            );

    }
}

const styles = StyleSheet.create({
    button: {
        marginVertical: 8,
        minWidth: "50%",
        backgroundColor: Colors.green,
        color: Colors.green,
    },
});



const mapStateToProps = state => {
    return {};vm
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CalendarOverviewScreen);
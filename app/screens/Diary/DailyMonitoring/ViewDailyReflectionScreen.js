import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {ScrollView, SectionList, StyleSheet, View} from 'react-native';
import {Text, Paragraph, FAB, ProgressBar, Surface, Divider} from "react-native-paper";
import Navigation from "../../../services/Navigation";
import {Typography} from "../../../styles";
import {Colors} from "../../../styles";

class ViewDailyReflectionScreen extends React.Component {

    // Diary data to display and pass to next screen
    data = {};

    constructor(props) {
        super(props);
        this.state = {};
        this.data = this.props?.route?.params.diary;
    }

    navigateToExperienceOverviewButton() {
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Abschliessen"
            accessibilityLabel="Abschliessen"
            onPress={() => Navigation.navigate("DiaryTab", "ExperienceOverview")}/>);
    }

    getCurrentDate() {
        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();
        today = yyyy + "-" + mm + "-" + dd;
        return today;
    }

    getPostList() {
        const postList = [
            {
                title: "Erleichternd",
                data: []
            },
            {
                title: "Erschwerend",
                data: []
            }
        ];
        let postsData = this.data.posts;
        postsData = postsData.filter(post => post.date_created.substring(0,10) === this.getCurrentDate());
        postsData.forEach(post => {
            if (post.positive) {
                let postForList = {
                    text: "👍: " + post.text
                };
                postList[0].data.push(postForList);
            } else {
                let postForList = {
                    text: "👎: " + post.text
                };
                postList[1].data.push(postForList);
            }
        })
        return postList;
    }

    listPositiveAndNegativePosts() {
        return (
            <ScrollView>
                <Surface style={styles.card}>
                    <SectionList
                        ItemSeparatorComponent={this.ItemSeparator}
                        sections={this.getPostList()}
                        renderItem={({item}) => <Text style={styles.item}>{item.text}</Text>}
                        keyExtractor={(item, index) => index}
                    />
                </Surface>
            </ScrollView>
        )
    }

    render() {
        const dailyObservationText = "Welche positiven und/oder negativen Erfahrungen hast du heute mit dem SelfCommitment gemacht?"
        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={{textAlign: 'center'}}>2 von 2</Text>
                <ProgressBar progress={1.0} style={{marginBottom: 20}}/>
                <Paragraph>{dailyObservationText}</Paragraph>
                <Divider style={{margin: 15}}/>
                {this.listPositiveAndNegativePosts()}
                {this.navigateToExperienceOverviewButton()}
                <Paragraph/>
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    item: {
        fontSize: 14,
        borderWidth: 0.2,
        borderColor: "#f2f2f2",
        borderRadius: 15,
        padding: 3,
        marginVertical: 2,
        backgroundColor: "#f2f2f2"
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    card: {
        margin: 10,
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 5,
    }
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ViewDailyReflectionScreen);
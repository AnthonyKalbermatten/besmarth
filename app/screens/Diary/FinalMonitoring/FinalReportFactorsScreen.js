import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, View, TouchableHighlight, ScrollView, SectionList, Alert} from 'react-native';
import {Surface, Text, Paragraph, FAB, ProgressBar, Divider} from "react-native-paper";
import Navigation from "../../../services/Navigation";
import {Colors} from "../../../styles";
import {createFactor, createFinalMonitoring, createHypothesis} from "../../../services/DiaryService";
import {createSharedContent} from "../../../services/SharedContentService";
import Checkbox from 'expo-checkbox';

class FinalReportFactorsScreen extends React.Component {

    diaryData = {};

    constructor(props) {
        super(props);
        this.state = {
            selected: [],
            factorData: []
        };
        this.diaryData = this.props?.route?.params.diary;
    }

    componentDidMount() {
        let factorList = this.getFactorList();
        let factorData = [];
        factorList[0].data.forEach(elem => factorData.push(elem));
        factorList[1].data.forEach(elem => factorData.push(elem));
        this.setState({factorData: factorData})
    }

    addRemoveItem(item) {
        if (!this.state.selected.includes(item)) {
            this.state.selected.push(item)
        } else {
            const index = this.state.selected.indexOf(item);
            this.state.selected.splice(index, 1);
        }
    }

    selectableFactors(item) {
        let isChecked = this.state.factorData.find(factor => factor.id == item.id)?.checked;
        return (
            <View style={styles.selectedRow}>
                <Checkbox
                    value={isChecked}
                    onValueChange={() => {
                        let factorData = this.state.factorData;
                        factorData.find(factor => factor.id == item.id).checked = !isChecked;
                        this.setState({
                            factorData: factorData
                        })
                        this.addRemoveItem(this.state.factorData.find(factor => factor.id == item.id));
                    }}/>
                <Text style={{marginTop: 5}}>{item.text}</Text>
            </View>);
    }

    getFactorList() {
        const factorList = [
            {
                title: "Erleichternd",
                data: []
            },
            {
                title: "Erschwerend",
                data: []
            }
        ];
        let factorsData = this.diaryData.factors.filter(factor => factor.finalFactor === false);
        factorsData.forEach(factor => {
            if (factor.positive) {
                let factorsForList = {
                    text: "👍: " + factor.text,
                    positive: true,
                    id: factor.id,
                    checked: false
                };
                factorList[0].data.push(factorsForList);
            } else {
                let factorsForList = {
                    text: "👎: " + factor.text,
                    positive: false,
                    id: factor.id,
                    checked: false
                };
                factorList[1].data.push(factorsForList);
            }
        })
        return factorList;
    }

    listPositiveAndNegativeFactors() {
        return (
            <ScrollView>
                <Surface style={styles.card}>
                    <SectionList
                        ItemSeparatorComponent={this.ItemSeparator}
                        sections={this.getFactorList()}
                        renderItem={({item}) => this.selectableFactors(item)}
                        keyExtractor={(item, index) => index}
                    />
                </Surface>
            </ScrollView>
        )
    }

    navigateToExperienceOverviewButton() {
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Zur Umfrage"
            accessibilityLabel="Abschliessen"
            onPress={() => this.challengeCompleted()}/>);
    }

    listActions() {
        return (
            <View>
                <View style={styles.actionViewDescription}>
                    <Text>Über diese Massnahmen hast du in den letzten vier Wochen nachgedacht.
                        Wenn du sie jetzt anschaust, was denkst du, kannst du als Individuum zu einer Nachhaltigen
                        Entwicklung beitragen und wo müssen wir gemeinsam Verantwortung übernehmen?
                        Wie kannst du diese gemeinsame Verantwortung mitgestalten – z.B. in deinem sozialen
                        Umfeld oder in deiner Gemeinde?
                    </Text>
                </View>
                <View style={styles.actionView}>
                    <Text style={styles.actiionTitles}>Massnahmen die man selbst umsetzen kann</Text>
                </View>
                <Surface style={styles.actionCard}>
                    <SectionList
                        ItemSeparatorComponent={this.ItemSeparator}
                        sections={this.getOwnActions()}
                        renderItem={({item}) => <Text style={styles.actionItem}>{item}</Text>}
                        keyExtractor={(item, index) => item + index}
                    />
                </Surface>
                <View style={styles.actionView}>
                    <Text style={styles.actiionTitles}>Massnahmen für Veränderung im sozialen Umfeld</Text>
                </View>
                <Surface style={styles.actionCard}>
                    <SectionList
                        ItemSeparatorComponent={this.ItemSeparator}
                        sections={this.getSocialActions()}
                        renderItem={({item}) => <Text style={styles.actionItem}>{item}</Text>}
                        keyExtractor={(item, index) => item + index}
                    />
                </Surface>
                <View style={styles.actionView}>
                    <Text style={styles.actiionTitles}>Massnahmen auf politischer Ebene</Text>
                </View>
                <Surface style={styles.actionCard}>
                    <SectionList
                        ItemSeparatorComponent={this.ItemSeparator}
                        sections={this.getPoliticalActions()}
                        renderItem={({item}) => <Text style={styles.actionItem}>{item}</Text>}
                        keyExtractor={(item, index) => item + index}
                    />
                </Surface>
                <View style={styles.actionView}>
                    <Text style={styles.actiionTitles}>Massnahmen auf der Ebene von Produktherstellern</Text>
                </View>
                <Surface style={styles.actionCard}>
                    <SectionList
                        ItemSeparatorComponent={this.ItemSeparator}
                        sections={this.getProductActions()}
                        renderItem={({item}) => <Text style={styles.actionItem}>{item}</Text>}
                        keyExtractor={(item, index) => item + index}
                    />
                </Surface>
            </View>
        );
    }

    getOwnActions() {
        const list = [
            {
                title: "ownActions",
                data: []
            }
        ];
        let ownActionsData = this.diaryData.weekly_monitorings;
        ownActionsData.forEach(weeklyMonitoring => {
            if (weeklyMonitoring.self_action != "") {
                list[0].data.push(weeklyMonitoring.self_action)
            }
        });
        return list;
    }

    getSocialActions() {
        const list = [
            {
                title: "socialActions",
                data: []
            }
        ];
        let socialActionsData = this.diaryData.weekly_monitorings;
        socialActionsData.forEach(weeklyMonitoring => {
            if (weeklyMonitoring.socialEnvironment_action != "") {
                list[0].data.push(weeklyMonitoring.socialEnvironment_action)
            }
        });
        return list;
    }

    getPoliticalActions() {
        const list = [
            {
                title: "politicalActions",
                data: []
            }
        ];
        let politicalActionsData = this.diaryData.weekly_monitorings;
        politicalActionsData.forEach(weeklyMonitoring => {
            if (weeklyMonitoring.political_action != "") {
                list[0].data.push(weeklyMonitoring.political_action)
            }
        });
        return list;
    }

    getProductActions() {
        const list = [
            {
                title: "productActions",
                data: []
            }
        ];
        let productActionsData = this.diaryData.weekly_monitorings;
        productActionsData.forEach(weeklyMonitoring => {
            if (weeklyMonitoring.producer_action != "") {
                list[0].data.push(weeklyMonitoring.producer_action)
            }
        });
        return list;
    }

    async challengeCompleted() {
        /*if (this.state.selected.length < 3 || this.state.selected.length > 5) {
            Alert.alert("Hinweis", "Bitte zwischen 3 und 5 wichtigsten Faktoren auswählen!")
            return
        }*/
        const factors = this.prepareFactorData();
        const diaryId = this.diaryData.id;
        await createHypothesis(diaryId, this.diaryData.endHypothesises);
        await factors.forEach(factor => {
            createFactor(diaryId, factor);
            //createSharedContent(this.prepareDataForFactorsSharedContentCreation(factor));
        })
        await createFinalMonitoring(diaryId);
        
        Navigation.navigate("DiaryTab", "FinalReportSurvey")
        this.props.showToast("Challenge erfolgreich abgeschlossen!");

    }

    prepareFactorData() {
        const factors = [];
        this.state.selected.forEach(factor => {
            let factorText = factor.text;
            factorText = factorText.substring(3)
            let minimalFactor = {
                text: factorText,
                positive: factor.positive,
                finalFactor: true,
                week: 4
            };
            factors.push(minimalFactor);
        })
        return factors;
    }

    prepareDataForFactorsSharedContentCreation = (factorElement) => {
        let data = {};
        data.contentText = factorElement.text;
        data.contentType = "FACTORS";
        return data;
    }


    prepareDataForActionsSharedContentCreation = (actionType, actionText) => {
        let data = {};
        data.contentText = actionType + ": " + actionText;
        data.contentType = "ACTIONS";
        return data;
    }



    render() {
        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={{textAlign: 'center'}}>3 von 4</Text>
                <ProgressBar progress={0.75} style={{marginBottom: 20}}/>
                <Paragraph>Was beeinflusste Dich bei der Einlösung Deines Self-Commitments? Wähle die <Text
                    style={{fontWeight: 'bold'}}>drei bis fünf</Text> wichtigsten Faktoren:</Paragraph>
                <Paragraph/>
                <Divider/>
                <Paragraph/>
                {this.listPositiveAndNegativeFactors()}
                <Paragraph/>
                <Divider/>
                <Paragraph/>
                {this.listActions()}
                <Paragraph/>
                {this.navigateToExperienceOverviewButton()}
                <Paragraph/>
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    selectedRow: {
        display: "flex",
        flexDirection: "row"
    },
    item: {
        fontSize: 14,
        borderWidth: 0.2,
        borderColor: "#f2f2f2",
        borderRadius: 15,
        padding: 3,
        marginVertical: 2,
        backgroundColor: "#f2f2f2"
    },
    actionItem: {
        fontSize: 14,
        borderWidth: 0.2,
        borderColor: "#f2f2f2",
        borderRadius: 15,
        padding: 3,
        paddingHorizontal: 5,
        marginVertical: 2,
        backgroundColor: "#f2f2f2"
    },
    selectedItem: {
        fontSize: 14,
        borderWidth: 0.2,
        borderColor: Colors.lightGreen,
        borderRadius: 15,
        padding: 3,
        marginVertical: 2,
        backgroundColor: Colors.lightGreen
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    card: {
        margin: 10,
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 5,
    },
    actionView: {
        paddingHorizontal: 10
    },
    actiionTitles: {
        fontWeight: "bold"
    },
    actionCard: {
        margin: 10,
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 5,
    },
    actionViewDescription: {
        paddingHorizontal: 10,
        paddingBottom: 10
    }
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FinalReportFactorsScreen);
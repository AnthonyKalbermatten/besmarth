import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, ScrollView} from 'react-native';
import {Text, Paragraph, FAB, ProgressBar} from "react-native-paper";
import Navigation from "../../../services/Navigation";
import {Colors} from "../../../styles";

class FinalReportIntroScreen extends React.Component {

    constructor(props) {
        super(props);
    }

    navigateToFinalReportButton() {
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Weiter"
            accessibilityLabel="Weiter"
            onPress={() => Navigation.navigate("DiaryTab", "FinalReport", {diary: this.props?.route?.params.diary})}/>);
    }

    computeDays() {
        let days = this.props?.route?.params.diary.challenge.duration;
        return days;
    }

    checkIfSelfCommitmentSuccessful() {
        const dailyMonitorings = this.props?.route?.params.diary.daily_monitorings;
        const challengeDuration = this.props?.route?.params.diary.challenge.duration;
        const successfulMessage = "befasst. Super, dass du durchgehalten hast! ";
        const failedMessage = "befasst. Super, dass du durchgehalten hast!";
        let successfulCounter = 0;

        dailyMonitorings.forEach(dailyMonitoring => {
            if(dailyMonitoring.done) successfulCounter++;
        })

        if(successfulCounter == challengeDuration) {
            return successfulMessage;
        } else {
            return failedMessage;
        }
    }

    render() {
        const selfCommitment = this.props?.route?.params.diary.selfCommitment;
        const days = this.computeDays();

        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={styles.progressBarText}>1 von 4</Text>
                <ProgressBar progress={0.25} style={styles.progressBar}/>
                <Paragraph>Du hast dich {days} Tage mit mit dem Self-Commitment</Paragraph>
                <Paragraph style={styles.selfCommitmentParagraph}>"{selfCommitment}"</Paragraph>

                <Paragraph>{this.checkIfSelfCommitmentSuccessful()}</Paragraph>
                <Paragraph/>
                <Paragraph>Zeit also nochmals abschliessend einen Blick auf deine Erfahrungen und Gedanken zu richten und zu überlegen wie du allein oder gemeinsam mit anderen an den Ideen für die Gestaltung einer nachhaltigeren und gerechteren Welt dranbleiben willst</Paragraph>
                {this.navigateToFinalReportButton()}
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    progressBarText: {
        textAlign: 'center'
    },
    progressBar: {
        marginBottom: 20
    },
    selfCommitmentParagraph: {
        textAlign: "center",
        paddingTop: 20,
        paddingBottom: 20,
        fontWeight: "bold"
    }
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FinalReportIntroScreen);
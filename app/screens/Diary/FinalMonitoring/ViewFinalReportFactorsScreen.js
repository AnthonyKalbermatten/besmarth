import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, View, ScrollView, SectionList} from 'react-native';
import {Surface, Text, Paragraph, FAB, ProgressBar, Divider} from "react-native-paper";
import Navigation from "../../../services/Navigation";
import {Colors} from "../../../styles";

class ViewFinalReportFactorsScreen extends React.Component {

    diaryData = {};

    constructor(props) {
        super(props);
        this.state = {};
        this.diaryData = this.props?.route?.params.diary;
    }

    getFactorList() {
        const factorList = [
            {
                title: "Erleichternd",
                data: []
            },
            {
                title: "Erschwerend",
                data: []
            }
        ];
        let factorsData = this.diaryData.factors.filter(factor => factor.finalFactor === true);
        factorsData.forEach(factor => {
            if (factor.positive) {
                let factorsForList = {
                    text: "👍: " + factor.text,
                    positive: true
                };
                factorList[0].data.push(factorsForList);
            } else {
                let factorsForList = {
                    text: "👎: " + factor.text,
                    positive: false
                };
                factorList[1].data.push(factorsForList);
            }
        })
        return factorList;
    }

    listPositiveAndNegativeFactors() {
        return (
            <ScrollView>
                <Surface style={styles.card}>
                    <SectionList
                        ItemSeparatorComponent={this.ItemSeparator}
                        sections={this.getFactorList()}
                        renderItem={({item}) => <Text style={styles.item}>{item.text}</Text>}
                        keyExtractor={(item, index) => index}
                    />
                </Surface>
            </ScrollView>
        )
    }

    navigateToExperienceOverviewButton() {
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Abschliessen"
            accessibilityLabel="Zur Umfrage"
            onPress={() => Navigation.navigate("DiaryTab", "FinalReportSurvey")}/>);
    }

    listActions() {
        return (
            <View>
                <View style={styles.actionViewDescription}>
                    <Text>Hier siehst du, was aufgrund deiner gemachten Erfahrungen konkret angestossen werden müsste... </Text>
                </View>
                <View style={styles.actionView}>
                    <Text style={styles.actiionTitles}>… bei dir selbst </Text>
                </View>
                <Surface style={styles.actionCard}>
                    <SectionList
                        ItemSeparatorComponent={this.ItemSeparator}
                        sections={this.getOwnActions()}
                        renderItem={({item}) => <Text style={styles.actionItem}>{item}</Text>}
                        keyExtractor={(item, index) => item + index}
                    />
                </Surface>
                <View style={styles.actionView}>
                    <Text style={styles.actiionTitles}>… in deinem sozialen Umfeld</Text>
                </View>
                <Surface style={styles.actionCard}>
                    <SectionList
                        ItemSeparatorComponent={this.ItemSeparator}
                        sections={this.getSocialActions()}
                        renderItem={({item}) => <Text style={styles.actionItem}>{item}</Text>}
                        keyExtractor={(item, index) => item + index}
                    />
                </Surface>
                <View style={styles.actionView}>
                    <Text style={styles.actiionTitles}>… auf politischer Ebene</Text>
                </View>
                <Surface style={styles.actionCard}>
                    <SectionList
                        ItemSeparatorComponent={this.ItemSeparator}
                        sections={this.getPoliticalActions()}
                        renderItem={({item}) => <Text style={styles.actionItem}>{item}</Text>}
                        keyExtractor={(item, index) => item + index}
                    />
                </Surface>
                <View style={styles.actionView}>
                    <Text style={styles.actiionTitles}>… auf der Ebene der Produktherstellung</Text>
                </View>
                <Surface style={styles.actionCard}>
                    <SectionList
                        ItemSeparatorComponent={this.ItemSeparator}
                        sections={this.getProductActions()}
                        renderItem={({item}) => <Text style={styles.actionItem}>{item}</Text>}
                        keyExtractor={(item, index) => item + index}
                    />
                </Surface>
            </View>
        );
    }

    getOwnActions() {
        const list = [
            {
                title: "ownActions",
                data: []
            }
        ];
        let ownActionsData = this.diaryData.weekly_monitorings;
        ownActionsData.forEach(weeklyMonitoring => {
            if (weeklyMonitoring.self_action != "") {
                list[0].data.push(weeklyMonitoring.self_action)
            }
        });
        return list;
    }

    getSocialActions() {
        const list = [
            {
                title: "socialActions",
                data: []
            }
        ];
        let socialActionsData = this.diaryData.weekly_monitorings;
        socialActionsData.forEach(weeklyMonitoring => {
            if (weeklyMonitoring.socialEnvironment_action != "") {
                list[0].data.push(weeklyMonitoring.socialEnvironment_action)
            }
        });
        return list;
    }

    getPoliticalActions() {
        const list = [
            {
                title: "politicalActions",
                data: []
            }
        ];
        let politicalActionsData = this.diaryData.weekly_monitorings;
        politicalActionsData.forEach(weeklyMonitoring => {
            if (weeklyMonitoring.political_action != "") {
                list[0].data.push(weeklyMonitoring.political_action)
            }
        });
        return list;
    }

    getProductActions() {
        const list = [
            {
                title: "productActions",
                data: []
            }
        ];
        let productActionsData = this.diaryData.weekly_monitorings;
        productActionsData.forEach(weeklyMonitoring => {
            if (weeklyMonitoring.producer_action != "") {
                list[0].data.push(weeklyMonitoring.producer_action)
            }
        });
        return list;
    }

    render() {
        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={{textAlign: 'center'}}>3 von 4</Text>
                <ProgressBar progress={0.75} style={{marginBottom: 20}}/>
                <Paragraph>Hier siehst du die Schlüsse, die du aus deiner Erfahrung mit dem Self-Commitment {this.props?.route?.params.diary.selfCommitment} gezogen hast. Wähle <Text
                    style={{fontWeight: 'bold'}}>drei bis fünf</Text> Schlüsse aus, die du auch über die Gruppe hinaus kommunizieren möchtest</Paragraph>
                <Paragraph/>
                <Divider/>
                <Paragraph/>
                {this.listPositiveAndNegativeFactors()}
                <Paragraph/>
                <Divider/>
                <Paragraph/>
                {this.listActions()}
                <Paragraph/>
                {this.navigateToExperienceOverviewButton()}
                <Paragraph/>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    item: {
        fontSize: 14,
        borderWidth: 0.2,
        borderColor: "#f2f2f2",
        borderRadius: 15,
        padding: 3,
        marginVertical: 2,
        backgroundColor: "#f2f2f2"
    },
    actionItem: {
        fontSize: 14,
        borderWidth: 0.2,
        borderColor: "#f2f2f2",
        borderRadius: 15,
        padding: 3,
        paddingHorizontal: 5,
        marginVertical: 2,
        backgroundColor: "#f2f2f2"
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    card: {
        margin: 10,
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 5,
    },
    actionView: {
        paddingHorizontal: 10
    },
    actiionTitles: {
        fontWeight: "bold"
    },
    actionCard: {
        margin: 10,
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 5,
    },
    actionViewDescription: {
        paddingHorizontal: 10,
        paddingBottom: 10
    }
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ViewFinalReportFactorsScreen);
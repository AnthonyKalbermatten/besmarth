import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {ScrollView, StyleSheet, View, Picker} from 'react-native';
import Slider from '@react-native-community/slider';
import {Text, Paragraph, Switch, FAB, ProgressBar, Divider, Surface} from "react-native-paper";
import Navigation from "../../services/Navigation";
import {Colors} from "../../styles";

class MonitoringCorrectionScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    getParticipationDates() {
        let dailyMonitorings = this.props?.route?.params.diary.daily_monitorings;;
        const timestampData = dailyMonitorings.map((timeStampItem) => timeStampItem.timestamp
            );
        console.log(dailyMonitorings[0].timestamp)
        console.log(timestampData[0])
        return timestampData;
    }

    getPickerDates()
    {
        let dates = this.getParticipationDates();
        console.log("Pickerdate:" + dates[0])
        /*const datePicker = dates.map( (date) =>
            {

                <Picker.Item label="Täglich" value="Täglich" />
                //<Picker.Item label="Täglich" value="DAILY"/>
                //<Picker.Item label={"value.topic_name"} value={3}/>
                ;
            })
        */

        let datePicker = dates.map((item,index) => {
            return ( <Picker.Item key={index} label={item} value={item} /> )
        })

        console.log(datePicker[0]);
        return datePicker;

    }


    render() {

        return (
            <ScrollView style={styles.contentWrapper}>

           <Picker
                    style={{height: 44}}
                    itemStyle={{height: 44}}
                    selectedValue={this.state.gender}
                    //onValueChange={val => Navigation.navigate("DiaryTab", "ViewDailyObservation", {diary: val})}}
                    >
                    <Picker.Item label="Datum auswählen" value="---"/>
                    {this.getPickerDates()}

            </Picker>
            </ScrollView>);

    }
}

const styles = StyleSheet.create({
    row: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        paddingTop: 15,
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    card: {
        margin: 10,
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 10,
    },
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MonitoringCorrectionScreen);
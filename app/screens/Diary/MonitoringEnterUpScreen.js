import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {ScrollView, StyleSheet, View} from 'react-native';
import {Text, Paragraph, Switch, FAB, ProgressBar, Divider, Surface, Button} from "react-native-paper";
import Navigation from "../../services/Navigation";
import {Colors} from "../../styles";

class MonitoringEnterUpScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }


    getDayToEnterUp = () => {
            let dailyMonitorings = this.props?.route?.params.diary.daily_monitorings
            let lastEntry = dailyMonitorings[dailyMonitorings.length -1];
            let dateOfLastEntry = lastEntry.timestamp;
            var dateLastEntry = new Date(dateOfLastEntry);
            dateLastEntry.setDate(dateLastEntry.getDate() + 1);
            let dateString = dateLastEntry.toString();
            dateString = dateString.slice(0,10);
            return <Text style={{fontWeight: "bold"}}>Du hast den Eintrag am {dateString} verpasst. </Text>
    }

    enterUpDetails = () => {

    }

    enterUpButton(){
            return <Button
                    style={styles.button}
                    mode="contained"
                    onPress={() => Navigation.navigate("DiaryTab", "DailyObservation", {diary: this.props?.route?.params.diary})}>
                    Tagebuch nachtragen
                </Button>
    }

    render() {

        return (
            <ScrollView style={styles.contentWrapper}>
            {this.getDayToEnterUp()}
           {this.enterUpDetails()}
           <Text>Mit einem Klick auf den Button kannst du dies nachholen.</Text>
           <Paragraph></Paragraph>
           {this.enterUpButton()}
            </ScrollView>);

    }
}


const styles = StyleSheet.create({
    row: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        paddingTop: 15,
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    card: {
        margin: 10,
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 10,
    },
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MonitoringEnterUpScreen);
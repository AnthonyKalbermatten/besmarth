import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, View, ScrollView} from 'react-native';
import {Text, Paragraph, FAB, ProgressBar, Divider} from "react-native-paper";
import Navigation from "../../../services/Navigation";
import {Colors} from "../../../styles";
import {Typography} from "styles/index"


class ViewWeeklyActionsScreen extends React.Component {

    data = {};

    constructor(props) {
        super(props);
        this.state = {};
        this.data = this.props?.route?.params.data;
    }

    navigateToExperienceOverviewButton() {
        return (<FAB
                style={styles.button}
                mode="contained"
                label="Abschliessen"
                accessibilityLabel="Abschliessen"
                onPress={() => Navigation.navigate("DiaryTab", "ExperienceOverview")}/>
        );
    }

    showActions() {
        const actions = this.data.diary.weekly_monitorings.find(weekly => weekly.week == this.data.week);
        return (
            <View>
                <Paragraph style={styles.actionTitle}>… bei dir selbst</Paragraph>
                <Paragraph>{actions.self_action}</Paragraph>
                <Paragraph />
                <Paragraph style={styles.actionTitle}>… in deinem sozialen Umfeld</Paragraph>
                <Paragraph>{actions.socialEnvironment_action}</Paragraph>
                <Paragraph />
                <Paragraph style={styles.actionTitle}>… auf politischer Ebene</Paragraph>
                <Paragraph>{actions.political_action}</Paragraph>
                <Paragraph />
                <Paragraph style={styles.actionTitle}>… auf der Ebene der Produktherstellung</Paragraph>
                <Paragraph>{actions.producer_action}</Paragraph>
                <Paragraph/>
            </View>
        );
    }

    render() {
        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={{textAlign: 'center'}}>3 von 3</Text>
                <ProgressBar progress={1.0} style={{marginBottom: 20}}/>
                <Paragraph>Was müsste aufgrund deiner gemachten Erfahrungen konkret angestossen werden?* </Paragraph>
                <Paragraph/>
                <Divider/>
                <Paragraph/>
                {this.showActions()}
                <Paragraph style={Typography.descriptionText}>* Diese Eingaben werden, falls du einer Gruppe zugehörst, mit dieser geteilt. Du kannst die Eingaben der anderen Gruppenmitglieder über das Menü Sharing einsehen und sie wissen lassen, wenn du ihre Beiträge gut findest.</Paragraph>
                <Paragraph/>
                {this.navigateToExperienceOverviewButton()}
                <Paragraph/>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    actionTitle: {
        fontWeight: 'bold'
    }
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ViewWeeklyActionsScreen);
import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, View, ScrollView} from 'react-native';
import {Headline, Text, Paragraph, Button, FAB, ProgressBar, Divider} from "react-native-paper";
import Navigation from "../../../services/Navigation";
import {Ionicons} from "@expo/vector-icons";
import {LineChart} from "react-native-chart-kit";
import {Dimensions} from "react-native";
import {Colors, Typography} from "../../../styles";

class WeeklyExperienceReportScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true
        };
    }

    navigateToWeeklyFactorsOverviewButton() {
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Weiter"
            accessibilityLabel="Weiter"
            onPress={() => Navigation.navigate("DiaryTab", "WeeklyFactorsOverview", {data: this.props?.route?.params})}/>);
    }

    getDiaryDataPerWeek() {
        switch (this.props?.route?.params.week) {
            case 1: return this.props?.route?.params.diary.daily_monitorings.slice(0, 7)
            case 2: return this.props?.route?.params.diary.daily_monitorings.slice(7, 14)
            case 3: return this.props?.route?.params.diary.daily_monitorings.slice(14, 21)
            case 4: return this.props?.route?.params.diary.daily_monitorings.slice(21, 28)
        }
    }

    getWellBeingData() {
        let dailyMonitorings = this.getDiaryDataPerWeek();
        let wellBeingData = [];
        dailyMonitorings.forEach(dailyMonitoring => {
            wellBeingData.push(parseFloat(dailyMonitoring.wellbeing))
        })
        return wellBeingData;
    }

    getWeekdaysDoneData() {
        let dailyMonitorings = this.getDiaryDataPerWeek();
        let doneData = [];
        dailyMonitorings.forEach(dailyMonitoring => {
            doneData.push(dailyMonitoring.done)
        })
        doneData.forEach(element => {
            if(element) {
                doneData[doneData.indexOf(element)] = "ios-checkmark-circle"
            } else {
                doneData[doneData.indexOf(element)] = "ios-close-circle"
            }
        })
        return (
            <View style={{...styles.row}}>
                <Button><Ionicons style={styles.rightIcon} name={doneData[0]} size={25}/></Button>
                <Button><Ionicons style={styles.rightIcon} name={doneData[1]} size={25}/></Button>
                <Button><Ionicons style={styles.rightIcon} name={doneData[2]} size={25}/></Button>
                <Button><Ionicons style={styles.rightIcon} name={doneData[3]} size={25}/></Button>
                <Button><Ionicons style={styles.rightIcon} name={doneData[4]} size={25}/></Button>
                <Button><Ionicons style={styles.rightIcon} name={doneData[5]} size={25}/></Button>
                <Button><Ionicons style={styles.rightIcon} name={doneData[6]} size={25}/></Button>
            </View>
        )
    }

    weekdaysDoneOverview() {
        return (<View>
                {this.getWeekdaysDoneData()}
                <View style={{...styles.row}}>
                    <Text>Tag 1</Text>
                    <Text>Tag 2</Text>
                    <Text>Tag 3</Text>
                    <Text>Tag 4</Text>
                    <Text>Tag 5</Text>
                    <Text>Tag 6</Text>
                    <Text>Tag 7</Text>
                </View>
            </View>
        )
    }

    weekWellBeing() {
        // TODO: LineChart from https://www.npmjs.com/package/react-native-chart-kit
        const data = {
            labels: ["Tag 1", "Tag 2", "Tag 3", "Tag 4", "Tag 5", "Tag 6", "Tag 7"],
            datasets: [
                {
                    data: this.getWellBeingData(),
                    color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`, // optional
                    strokeWidth: 2 // optional
                }
            ],
        };
        const screenWidth = Dimensions.get("window").width*1.2; // hack to hide yAxis
        const chartConfig = {
            backgroundGradientFrom: "#1E2923",
            backgroundGradientFromOpacity: 0,
            backgroundGradientTo: "#08130D",
            backgroundGradientToOpacity: 0.5,
            color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
            strokeWidth: 2, // optional, default 3
            barPercentage: 0.5,
            useShadowColorFromDataset: false // optional
        };
        return (<View style={{...styles.row}}>
            <LineChart
                withInnerLines={false}
                data={data}
                width={screenWidth}
                bezier
                height={220}
                chartConfig={chartConfig}
                style={{marginRight: 8}}
            />
        </View>)
    }

    render() {
        const weekNr = this.props?.route?.params.week

        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={{textAlign: 'center'}}>1 von 3</Text>
                <ProgressBar progress={0.33} style={{marginBottom: 20}}/>
                <Headline style={Typography.descTitle}>Meine {weekNr}. Woche mit dem Self-Commitment </Headline>
                <Paragraph>Tage mit eingelöstem Self-Commitment:</Paragraph>
                {this.weekdaysDoneOverview()}
                <Paragraph/>
                <Divider/>
                <Paragraph/>
                {this.weekWellBeing()}
                <Paragraph/>
                {this.navigateToWeeklyFactorsOverviewButton()}
                <Paragraph/>
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    row: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(WeeklyExperienceReportScreen);
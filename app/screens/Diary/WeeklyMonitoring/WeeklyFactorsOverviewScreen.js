import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, ScrollView, View, SectionList, Dimensions, Alert} from 'react-native';
import {Text, Paragraph, Surface, Button, FAB, TextInput, ProgressBar, Divider} from "react-native-paper";
import Navigation from "../../../services/Navigation";
import {Ionicons} from "@expo/vector-icons";
import {Colors} from "../../../styles";
import {Typography} from "styles/index"


class WeeklyFactorsOverviewScreen extends React.Component {

    data = {};

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            summary: "",
            summaryList: [
                {
                    title: "Erleichternd",
                    data: []
                },
                {
                    title: "Erschwerend",
                    data: []
                }
            ],
            minimalFactorList: []
        };
        this.data = this.props?.route?.params.data;
    }

    navigateToWeeklyActionsButton() {
        this.data.factors = this.state.minimalFactorList;
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Weiter"
            accessibilityLabel="Weiter"
            onPress={() => this.alertForOpenSummaryOrNavigateToNextScreen()}/>);
    }

    alertForOpenSummaryOrNavigateToNextScreen() {
        if(this.state.summary !== "") {
            Alert.alert("Hinweis", "Zuerst den noch offenen Post anhand der ! oder ? Symbole abschicken! Sonst geht der Post verloren")
        } else if (this.state.minimalFactorList.length === 0) {
            Alert.alert("Hinweis", "Benenne mindestens einen ? oder ! Faktor, um fortzufahren.")
        } else {
            Navigation.navigate("DiaryTab", "WeeklyActions", {data: this.data})
        }
    }

    getPostList() {
        const postList = [
            {
                title: "Erleichternd",
                data: []
            },
            {
                title: "Erschwerend",
                data: []
            }
        ];
        let postsData = this.data.diary.posts;
        postsData = postsData.filter(post => post.week === this.data.week);
        postsData.forEach(post => {
            if (post.positive) {
                let postForList = {
                    text: "❓: " + post.text
                };
                postList[0].data.push(postForList);
            } else {
                let postForList = {
                    text: "❗: " + post.text
                };
                postList[1].data.push(postForList);
            }
        })
        return postList;
    }

    listPositiveAndNegativePosts() {
        return (
            <ScrollView>
                <Surface style={styles.card}>
                    <SectionList
                        ItemSeparatorComponent={this.ItemSeparator}
                        sections={this.getPostList()}
                        renderItem={({item}) => <Text style={styles.item}>{item.text}</Text>}
                        keyExtractor={(item, index) => index}
                    />
                </Surface>
            </ScrollView>
        )
    }

    onChangeField(fieldName, newValue) {
        this.setState({
            ...this.state,
            [fieldName]: newValue
        });
    }

    summaryList() {
        if (this.state.minimalFactorList.length !== 0) {
            return (
                <ScrollView>
                    <Surface style={styles.card}>
                        <SectionList
                            ItemSeparatorComponent={this.ItemSeparator}
                            sections={this.state.summaryList}
                            renderItem={({item}) => <Text style={styles.item}>{item.text}</Text>}
                            keyExtractor={(item, index) => index}
                        />
                    </Surface>
                </ScrollView>
            )
        }
    }

    addPositiveSummary() {
        //Add minimalFactor for backend
        let minimalFactor = {};
        minimalFactor.text = this.state.summary;
        minimalFactor.positive = true;
        minimalFactor.finalFactor = false;
        minimalFactor.week = this.data.week;
        this.state.minimalFactorList.push(minimalFactor);
        //Add summary for frontend to summaryList
        let summary = {};
        summary.text = "❓: " + this.state.summary;
        let summaryList = this.state.summaryList;
        summaryList[0].data.push(summary);
        this.setState({...this.state, summary: "", postList: summaryList});
    }

    addNegativeSummary() {
        //Add minimalFactor for backend
        let minimalFactor = {};
        minimalFactor.text = this.state.summary;
        minimalFactor.positive = false;
        minimalFactor.finalFactor = false;
        minimalFactor.week = this.data.week;
        this.state.minimalFactorList.push(minimalFactor);
        //Add summary for frontend to summaryList
        let summary = {};
        summary.text = "❗: " + this.state.summary;
        let summaryList = this.state.summaryList;
        summaryList[1].data.push(summary);
        this.setState({...this.state, summary: "", postList: summaryList});
    }

    writeOwnSummaryOfWeek() {
        return (
            <View style={styles.row}>
                <TextInput
                    style={styles.formControl}
                    label="Meine Gedanken…"
                    value={this.state.summary}
                    accessibilityLabel="Meine Gedanken…"
                    onChangeText={val => this.onChangeField("summary", val)}
                />
                <Button disabled={this.state.summary === ""} onPress={() => this.addPositiveSummary()}><Ionicons
                    style={styles.rightIcon} name={"help"}
                    size={25}/></Button>
                <Button disabled={this.state.summary === ""} onPress={() => this.addNegativeSummary()}><Ionicons
                    style={styles.rightIcon} name={"alert"}
                    size={25}/></Button>
            </View>
        );
    }

    render() {
        const selfCommitment = this.data.diary.selfCommitment;

        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={{textAlign: 'center'}}>2 von 3</Text>
                <ProgressBar progress={0.66} style={{marginBottom: 20}}/>
                <Paragraph>Tagebucheinträge Self-Commitment "{selfCommitment}":</Paragraph>
                {this.listPositiveAndNegativePosts()}
                <Paragraph/>
                <Divider/>
                <Paragraph/>
                <Paragraph>Welche Erfahrungen, welche Fragen haben dich diese Woche im Zusammenhang mit deinem Self-Commimtent umgetrieben? Und welche Schlüsse kannst du daraus ziehen, die du an dieser Stelle auch anderen weitergeben möchtest?*</Paragraph>
                {this.summaryList()}
                {this.writeOwnSummaryOfWeek()}
                <Paragraph style={Typography.descriptionText}>* Diese Eingaben werden, falls du einer Gruppe zugehörst, mit dieser geteilt. Du kannst die Eingaben der anderen Gruppenmitglieder über das Menü Sharing einsehen und sie wissen lassen, wenn du ihre Beiträge gut findest.</Paragraph>
                {this.navigateToWeeklyActionsButton()}
                <Paragraph/>
            </ScrollView>);
    }
}

const {width, height} = Dimensions.get("window");
const styles = StyleSheet.create({
    row: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center"
    },
    sectionHeader: {
        fontSize: 15,
        fontWeight: 'bold',
    },
    item: {
        fontSize: 14,
        borderWidth: 0.2,
        borderColor: "#f2f2f2",
        borderRadius: 15,
        padding: 3,
        marginVertical: 2,
        backgroundColor: "#f2f2f2"
    },
    formControl: {
        marginVertical: 10,
        width: width / 1.5
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    card: {
        margin: 10,
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 5,
    },
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(WeeklyFactorsOverviewScreen);
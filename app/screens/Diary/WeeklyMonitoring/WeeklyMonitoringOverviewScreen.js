import React from "react";
import {FlatList, StyleSheet, View, Dimensions, ScrollView} from "react-native";
import {bindActionCreators} from "redux";
import {Button, Headline, Surface, Text} from "react-native-paper";
import {Ionicons} from "@expo/vector-icons";
import {connect} from "react-redux";
import Navigation from "../../../services/Navigation";
import {Colors} from "../../../styles";

class WeeklyMonitoringOverviewScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
        };
    }

    render() {
        if (this.state.error) {
            return <View style={styles.container}><Text>{this.state.error}</Text></View>;
        }

        const emptyContent = <Headline style={styles.emptyListWrapper}>Keine Wöchentliche Rückblicke verfügbar.</Headline>;

        return (
            <View style={styles.container}>
                <FlatList
                    testID="weekly-monitoring-list"
                    data={this.props?.route?.params.diaryWRONGDATA}
                    refreshing={this.state.loading}
                    keyExtractor={(item) => item.id + ""}
                    ListEmptyComponent={() => emptyContent}
                    renderItem={({item}) =>
                        <DiaryItem
                            participation={item}
                        />
                    }
                />
            </View>);
    }
}

const checkForWeekButton = (dailyMonitorings, week) => {
    switch (week) {
        case 1:
            return (dailyMonitorings.length < 7)
        case 2:
            return (dailyMonitorings.length < 14)
        case 3:
            return (dailyMonitorings.length < 21)
        case 4:
            return (dailyMonitorings.length < 28)
    }
}

const checkForWeeklyMonitoring = (dailyMonitorings, weeklyMonitorings) => {
    //needs to be revised, ugly implementation. Three cases. When seven days have passed, force user to make weeklyMonitoring but stop blocking when it is done. Case 0: allow user to make first entry
    if(dailyMonitorings.length % 7 === 0 && (weeklyMonitorings.length < dailyMonitorings.length/7) && dailyMonitorings.length != 0) return true
    else return false
}

const checkForFinalMonitoring = weeklyMonitorings => {
    return (!(weeklyMonitorings.length >= 4));
}

const getTodaysDate = () => {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();
    today = yyyy + "-" + mm + "-" + dd;
    return today
}

const checkDailyMonitoring = dailyMonitorings => {
    let result = dailyMonitorings.find(dailyMonitoring => dailyMonitoring.timestamp === getTodaysDate());
    return (result !== undefined);
}

function DiaryItem({participation}) {
    console.log(participation)

    let weeklyButton = (weekNr) => {

            return <Button
                disabled={checkForWeekButton(participation.daily_monitorings, weekNr)}
                style={styles.button}
                mode="contained"
                onPress={() => Navigation.push("DiaryTab", "WeeklyExperienceReport", {
                    diary: participation,
                    week: weekNr
                })}>Woche {weekNr}</Button>
        
    }

    let finalButton = () => {
        if (participation.final_monitoring.length == 1) {
            return <Button
                disabled={checkForFinalMonitoring(participation.weekly_monitorings)}
                style={styles.button}
                mode="contained"
                onPress={() => Navigation.navigate("DiaryTab", "ViewFinalReportIntro", {diary: participation})}>
                Rückblick <Ionicons style={styles.checkmark} name="ios-checkmark-circle"/>
            </Button>
        } else {
            return <Button
                disabled={checkForFinalMonitoring(participation.weekly_monitorings)}
                style={styles.button}
                mode="contained"
                onPress={() => Navigation.navigate("DiaryTab", "FinalReportIntro", {diary: participation})}>
                Finaler Rückblick
            </Button>
        }
    }

    return (
        <Surface style={styles.swipeCard}>
            <View style={styles.cardTitle}>
                <View style={{flex: 1}}>
                    <Headline>Wöchentliche Rückblicke</Headline>
                </View>
            </View>
            <View style={styles.selfCommitment}>
                <Text>{participation.selfCommitment}</Text>
            </View>
            <ScrollView>
                {weeklyButton(1)}
                {weeklyButton(2)}
                {weeklyButton(3)}
                {weeklyButton(4)}
                {finalButton()}
            </ScrollView>
        </Surface>);
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "row",
    },
    swipeCard: {
        borderRadius: 50,
        elevation: 10,
        padding: 10,
        margin: 30,
        marginTop: 20,
        width: Dimensions.get('window').width - 60,
        height: "92%"
    },
    cardTitle: {
        display: "flex",
        flexDirection: "row",
        alignItems: "flex-start",
        justifyContent: "flex-end"
    },
    category: {
        backgroundColor: Colors.green,
        borderRadius: 10,
        color: "white",
        paddingVertical: 3,
        paddingHorizontal: 10,
    },
    button: {
        marginVertical: 8,
        minWidth: "50%"
    },
    participationContent: {
        flex: 1,
        display: "flex",
        flexDirection: "row",
        alignItems: "flex-end",
        marginTop: 10
    },
    emptyListWrapper: {
        textAlign: "center",
        width: "90%",
        paddingHorizontal: 15,
        marginTop: "25%"
    },
    checkmark: {
        fontSize: 22,
        padding: 0
    },
    nextButton: {
        marginVertical: 4,
        minWidth: "50%"
    },
    selfCommitment: {
        alignItems: "center",
    },
    selfCommitmentHeader: {
        alignItems: "center",
        paddingTop: 10
    },
    selfCommitmentHeaderText: {
        fontWeight: "bold"
    }
});

const mapStateToProps = state => {
    let accountId;
    if (state.account.account.id === undefined) {
        accountId = state.account.account.user_id;
    } else {
        accountId = state.account.account.id;
    }
    return {accountId};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(WeeklyMonitoringOverviewScreen);

import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, View, ScrollView, Linking, Image} from 'react-native';
import {Text, Paragraph, Switch, FAB} from "react-native-paper";
import Navigation from "../services/Navigation";
import {Colors} from "../styles";
import RemoteStorage from "../services/RemoteStorage";
import {isProductionMode} from "../services/utils";
import {ACCOUNT_API_PATH} from "../services/AccountService";

class InfotrailerScreen extends React.Component {

    constructor(props) {
        super(props);
    }

    navigateToHomeScreen() {
        if (this.props.route.params?.previous_screen != "ManageAccount") {
            return (
            <FAB
                style={styles.button}
                mode="contained"
                label="Los Geht's"
                onPress={() => {
                    if (this.props.signedIn) {
                        Navigation.navigateScreen("Initial")
                    } else {
                        Navigation.navigateScreen("SignIn")
                    }
                }}/>);
        }
    }

    render() {
        const infotrailerText = "Ist dir eine nachhaltigere und gerechtere Welt ein Anliegen? Bist du überzeugt, dass du bereits einen wichtigen Beitrag dazu leistest oder denkst du eher, dass dein Beitrag ruhig noch etwas grösser sein könnte?\nEgal wo genau du stehst, diese App hilft dir, über deinen Beitrag zu einer nachhaltigeren und gerechteren Welt nachzudenken.\n\n" +
            "Führe während vier Wochen Tagebuch, wie es dir damit geht, wenn du deinen Strom oder Wasserverbrauch reduzierst oder dich vegetarisch ernährst. Die App unterstützt dich dabei, deine Erfahrungen festzuhalten, zu reflektieren und am Ende jeder Woche die Erkenntnisse aus deinen Beobachtungen mit deiner Gruppe zu teilen. So könnt ihr in der Gruppe gemeinsam an euren Ideen für die Gestaltung einer nachhaltigeren und gerechteren Welt dranbleiben.\n\n" +
            "Du wählst selbst aus, welchen Bereich du in den nächsten vier Wochen nachhaltiger gestalten und welches sogenannte Self-Commitment du in dieser Zeit eingehen möchtest. Bist du dabei?\n\n" +
            "Mit der Nutzung der App erklärst du dich einverstanden damit, dass die von dir eingegebenen Daten anonym im Rahmen einer Studie an der PH FHNW ausgewertet und für die Optimierung der App genutzt werden dürfen."
        return (
            <ScrollView style={styles.contentWrapper}>
                <Image source={require('../assets/images/infotrailer.jpg')} style={styles.mapImage}/>
                <Paragraph>{infotrailerText}</Paragraph>
                {this.navigateToHomeScreen()}
                <FAB
                    style={styles.button}
                    mode="contained"
                    label="Datenschutzerklärung"
                    onPress={() => Linking.openURL("http://86.119.43.87/privacy-policy/")}/>
                    <Paragraph />
                    <Paragraph />
                    <Paragraph />
                    <Paragraph />
                    <Paragraph />
                    <Paragraph />
                    <Paragraph />
                    <Paragraph />
                    <Paragraph />
                    <Paragraph />
                    <Paragraph />
                    <Paragraph />
                    <Paragraph />
                    <Paragraph />
            </ScrollView>)
    }
}

const styles = StyleSheet.create({
    contentWrapper: {
        paddingHorizontal: 10,
        paddingTop: 5,
    },
    button: {
        marginHorizontal: 10,
        marginTop: 10,
        backgroundColor: Colors.green,
    },
    mapImage: {
        height: "25%",
        width: "100%",
        borderRadius: 10,
    },
});

const mapStateToProps = state => {
    return {
        signedIn: !state.account.account.pseudonymous
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(InfotrailerScreen);
import React from "react";
import {Dimensions, FlatList, RefreshControl, ScrollView, StyleSheet, View} from "react-native";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Button, Headline, IconButton, Paragraph, Searchbar, Text, TouchableRipple} from "react-native-paper";
import Navigation from "../../services/Navigation";
import {showToast} from "../../services/Toast";
import SegmentedControlTab from "react-native-segmented-control-tab";
import {
    acceptFriendRequest,
    createFriendRequest,
    deleteFriend,
    deleteFriendRequest,
    fetchFriends,
    fetchIncomingFriendRequests,
    fetchOutgoingFriendRequests,
    searchUser
} from "../../services/FriendService";
import {fetchGroupMembers, createGroupInvitation, deleteGroupAffiliation, deleteGroup } from "../../services/GroupsharingService";
import debounce from "debounce";
import {notNull} from "../../services/utils";
import {Colors} from "../../styles/index";

// Index Vor Tabbed View
const SEARCH_VIEW = 1;
const REQUEST_INCOMING_VIEW = 0;

// Values for User Type Map
const UT_FRIEND = "friend";
const UT_FRIEND_REQUEST_SENT = "request_sent";
const UT_FRIEND_REQUEST_INCOMING = "request_incoming";

class GroupMembersScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            selectedIndex: 0,
            friendSearch: "",
            friends: [],
            strangers: [],
            groupMembers: [],
            groupName: "",
            group_id: [],
            users: [],
        };
    }

    // Setting the index of SegmentedControlTab
    handleIndexChange = index => {
        this.setState({
            ...this.state,
            selectedIndex: index
        });
    };


    /**
     * Redirects the user to SignIn screen if he is not signed in
     */
    componentDidMount() {
        if (this.props.signedIn) {
        }

        this.navFocusListenerSub = this.props.navigation.addListener("focus", this.onScreenFocus.bind(this));
    }

    componentWillUnmount() {
        this.navFocusListenerSub();
    }

    /**
     * Called when the user focuses this screen
     */
    async onScreenFocus() {
        if (notNull(this.props?.route?.params?.scannedCode)) {
            this.onSearch(this.props?.route?.params?.scannedCode);
            this.props.navigation.setParams({scannedCode: null});
            // do not reload, this is already done by onSearch
        } else {
            this.reload().then();
        }
    }

    async reload() {
        if (!this.props.signedIn) {
            // do not load anything if not signed in
            return;
        }
        this.setState({
            ...this.state,
            loading: true,
        });

        this.searchUsersImmediately(this.state.friendSearch);
    }

    openScanner() {
        Navigation.navigate("FriendsTab", "ScanFriendCode");
    }

    async onSearch(value) {
        this.setState({
            ...this.state,
            friendSearch: value,
        });

        // searchUsers will be delayed for 300ms, to reduce calls to searchUsers
        await this.searchUsers(value);
    }

    // immediately fires off a reload
    searchUsersImmediately = async (username) => {
        console.log(this.props?.route?.params.groupAffiliation.group)
        username = username.toLocaleLowerCase();
        try {

            // Get Friends and co. data
            const responseFriends = await fetchFriends();
            const responseRequestSent = await fetchOutgoingFriendRequests();
            const responseRequestIncoming = await fetchIncomingFriendRequests();

            // Data Lists for Friends and co.
            const friends = responseFriends.data.filter(s => s.username.toLocaleLowerCase().includes(username));
            const requestSent = responseRequestSent.data.map(o => o.receiver);
            const requestIncoming = responseRequestIncoming.data.map(o => o.sender);

            //get all group affiliations for this group with the group id
            const responseGroupMembers = await fetchGroupMembers(this.props?.route?.params.groupAffiliation.group)
            const groupMembers = responseGroupMembers.data;

            const responseUsers = await searchUser("");
            const users = responseUsers.data;


            // Populate Friends and co. in userTypeMap
            const userTypeMap = new Map();

            friends.forEach((f) => userTypeMap.set(f.id, UT_FRIEND));
            requestSent.forEach((u) => userTypeMap.set(u.id, UT_FRIEND_REQUEST_SENT));
            requestIncoming.forEach((u) => userTypeMap.set(u.id, UT_FRIEND_REQUEST_INCOMING));

            let strangers = [];
            if (username.length >= 3) {
                const searchResponse = await searchUser(username);
                // Remove friends and logged in user
                strangers = searchResponse.data.filter(s => {
                        const isFriend = userTypeMap.get(s.id) !== UT_FRIEND;
                        const isCurrentUser = s.username !== this.props.username;
                        return isFriend && isCurrentUser;
                    }
                );
            }

            this.setState({
                ...this.state,
                loading: false,
                error: null,
                friends,
                strangers,
                requestSent,
                requestIncoming,
                userTypeMap,
                groupMembers,
                users,
            });

        } catch (e) {
            this.setState({
                    error: JSON.stringify(e),
                    loading: false
                }
            );
        }
    };


    // to be called by search,  defers actual reload for 300ms
    searchUsers = debounce(this.searchUsersImmediately, 300);

    async sendGroupInvitation(user) {
        receiverData = {}
        try {
            receiverData.receiver_id = user.id;
            await createGroupInvitation(receiverData, this.props?.route?.params.groupAffiliation.group);
            this.props.showToast("Mitglied eingeladen.");
            this.reload();
        } catch (e) {
            console.error(e);
            this.setState({
                    error: JSON.stringify(e),
                    loading: false,
                }
            );
        }
    }
    async deleteGroup(groupId) {
        try {
            await deleteGroup(groupId);
            this.props.showToast("Gruppe gelöscht");
            Navigation.navigate("SharingTab", "SharingOverview")
        } catch (e) {
            console.error(e);
            this.setState({
                    error: JSON.stringify(e),
                    loading: false,
                }
            );
        }
    }


    async kickFromGroup(affiliation_instance) {
        try {
            await deleteGroupAffiliation(affiliation_instance.group, affiliation_instance.id);
            this.props.showToast("Nutzer entfernt.");
            this.reload();
        } catch (e) {
            console.error(e);
            this.setState({
                    error: JSON.stringify(e),
                    loading: false,
                }
            );
        }        
    }

    // -------------------
    // UI Stuff
    // -------------------

    render() {
        if (this.state.error != null) {
            return (<View style={styles.container}>
                <Text>Sorry, ein Fehler ist aufgetreten</Text>
                <Text>{this.state.error}</Text>
            </View>);
        }

        if (!this.props.signedIn) {
            return (
                <View style={[{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    marginHorizontal: 20
                }, styles.container]}>
                    <Paragraph style={{textAlign: "center"}}>
                        Du musst registriert sein um diesen Screen zu sehen.
                    </Paragraph>
                    <Button style={styles.button} mode="contained"
                            onPress={() => Navigation.navigate("AccountTab", "SignIn")}>Login</Button>
                    <Paragraph style={{marginVertical: 10}}>oder</Paragraph>
                    <Button mode="contained" style={{marginVertical: 20}} accessibilityLabel="Jetzt registrieren"
                            onPress={() => Navigation.navigate("AccountTab", "Infotrailer")}>
                        Jetzt registieren
                    </Button>
                </View>
            );
        }

        let viewPane;
        switch (this.state.selectedIndex) {
            case 1:
                viewPane = this.renderGroupMembersScreen();
                break;
            case 2:
                viewPane = this.renderSearchScreen();
                break;
            default:
                viewPane = this.renderSearchScreen();
        }

        let renderSearchField;
        if (this.state.selectedIndex === 1) {
            renderSearchField = (<View style={styles.searchView}>
                <Searchbar
                    accessibilityLabel="Suchleiste"
                    style={styles.searchField}
                    placeholder="Benutzername..."
                    value={this.state.friendSearch}
                    onChangeText={this.onSearch.bind(this)}
                />
            </View>);
        }


        return (
            <View style={styles.container}>
                <ScrollView
                    keyboardShouldPersistTaps="handled" /* when keyboard is open, allow button press */
                    refreshControl={<RefreshControl refreshing={this.state.loading}
                                                    onRefresh={this.reload.bind(this)}/>}>

                    <SegmentedControlTab
                        values={["Mitglieder", "Admin-Bereich"]}
                        tabStyle={{backgroundColor: Colors.veryLightGray, borderWidth: 1.4, borderColor: Colors.green, elevation: 5}}
                        activeTabStyle={{backgroundColor: Colors.green}}
                        tabTextStyle={{color: Colors.black, padding: 3}}
                        selectedIndex={this.state.selectedIndex}
                        onTabPress={this.handleIndexChange}
                    />

                    {renderSearchField}
                    {viewPane}
                    
                </ScrollView>
            </View>

        );
    }

    navigateToUser = (user) => {
        Navigation.navigate("FriendsTab", "FriendDetail", {
            friend: user
        });
    };

    getUserNameFromAffiliation(affiliation_instance){
        let member_instance = this.state.users.find(element => element.id == affiliation_instance.member);
        console.log(member_instance);
        return member_instance.username;
    }

    /**
     * Renders a FlatList with the given data as renderItems.
     * The buttons will be displayed on each item.
     *
     * @param data Data to be displayed
     * @param createButtonFunction Function which takes a userId as Argument
     * @returns FlatList
     */

     renderFlatList(data, createButtonFunction) {
        return (
            <FlatList
                keyboardShouldPersistTaps="handled"
                data={data}
                refreshing={this.state.loading}
                onRefreash={this.reload.bind(this)}
                renderItem={({item}) => (
                    <TouchableRipple accessibilityLabel={"Benutzer - " + item.username}
                                     style={styles.itemTouchableContainer}
                                     onPress={() => this.navigateToUser(item)}>
                        <View style={styles.itemContainer}>
                            <Text style={styles.username}>{item.username}</Text>
                            <View style={styles.buttonContainer}>
                                {createButtonFunction(item)}
                            </View>
                        </View>
                    </TouchableRipple>
                )}
                keyExtractor={item => item.id + ""}
            />
        );
    }

    renderMemberFlatList(data, createButtonFunction) {
        return (
            <FlatList
                keyboardShouldPersistTaps="handled"
                data={data}
                refreshing={this.state.loading}
                onRefreash={this.reload.bind(this)}
                renderItem={({item}) => (
                    <TouchableRipple accessibilityLabel={"Benutzer - " + item.username}
                                     style={styles.itemTouchableContainer}
                                     onPress={() => {}}>
                        <View style={styles.itemContainer}>
                            <Text style={styles.username}>{this.getUserNameFromAffiliation(item)}</Text>
                            <View style={styles.buttonContainer}>
                                {createButtonFunction(item)}
                            </View>
                        </View>
                    </TouchableRipple>
                )}
                keyExtractor={item => item.id + ""}
            />
        );
    }


    // -------------------
    // Buttons
    // -------------------
    sendGroupInvitationButton = (userId) => (
        <IconButton small
                    icon="account-plus"
                    color={Colors.lightGreen}
                    accessibilityLabel={"User einladen - " + userId}
                    onPress={() => this.sendGroupInvitation(userId)}
                    key={userId + "inviteUser"}
        />
    );

    kickMemberButton = (userId) => (
        <IconButton small
                    icon="account-off"
                    color={Colors.red}
                    accessibilityLabel={"User entfernen - " + userId}
                    onPress={() => this.kickFromGroup(userId)}
                    key={userId + "removeFriend"}
        />
    );

    // -------------------
    // Screens
    // -------------------
    renderGroupMembersScreen() {

        const strangerData = this.state.strangers;
        const sendGroupInvitationButton = this.sendGroupInvitationButton

        let strangersList;
        if (this.state.strangers.length > 0) {
            strangersList = (<View>
                <Headline style={styles.heading}>Nutzer hinzufügen</Headline>
                {this.renderFlatList(strangerData, sendGroupInvitationButton)}
            </View>);
        }

        return (
            <View>
                {strangersList}
                <Paragraph></Paragraph>
                <Paragraph></Paragraph>
                <Button style={styles.button} onPress={() => this.deleteGroup(this.props?.route?.params.groupAffiliation.group)}>Gruppe löschen</Button>
            </View>
        );


    }

    renderSearchScreen() {
        const memberData = this.state.groupMembers;
        const kickUserButton = this.kickMemberButton;

        return (
            <View>
                <Headline style={styles.special_heading}>{this.props?.route?.params.groupName}</Headline>
                <Headline style={styles.heading}>Gruppenmitglieder</Headline>
                {this.renderMemberFlatList(memberData, kickUserButton)}

            </View>
        );

 
    }

}

const {width, height} = Dimensions.get("window");
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    searchField: {
        marginTop: 20,
        marginLeft: 5,
        maxWidth: width/1.25,
    },
    itemTouchableContainer: {
        marginTop: 8,
        marginBottom: 8,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 10,
        borderBottomColor: "lightgrey",
        borderTopColor: "lightgrey",
        borderBottomWidth: 1,
        borderTopWidth: 1,
        height: 60,
        display: "flex",
        justifyContent: "center"
    },
    itemContainer: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },
    buttonContainer: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },
    heading: {
        marginTop: 30,
        marginBottom: 10,
        textAlign: "center",
    },
    special_heading : {
        marginTop: 30,
        marginBottom: 10,
        textAlign: "center",
        fontWeight: "bold"
    },
    username: {
        fontSize: 18,
    },
    searchView: {
        display: "flex",
        flexDirection: "row",
        alignItems: "flex-end",
    },
    scannerToggler: {}
});

const mapStateToProps = state => {
    return {
        signedIn: !state.account.account.pseudonymous,
        username: state.account.account.username
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({
    showToast,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(GroupMembersScreen);
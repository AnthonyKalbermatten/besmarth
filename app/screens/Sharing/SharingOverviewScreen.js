import React from "react";
import {AppRegistry, Dimensions, FlatList, RefreshControl, ScrollView, StyleSheet, View} from "react-native";
import {bindActionCreators, combineReducers} from "redux";
import {connect} from "react-redux";
import {Button, Headline, IconButton, Paragraph, TextInput, Text, TouchableRipple, Divider, Caption} from "react-native-paper";
import Navigation from "../../services/Navigation";
import {showToast} from "../../services/Toast";
import SegmentedControlTab from "react-native-segmented-control-tab";
import debounce from "debounce";
import {notNull} from "../../services/utils";
import {Colors} from "../../styles/index";
import {
    createGroup,
    createGroupAffiliation,
    fetchGroups,
    fetchGroupInvitationsUser,
    fetchAffiliatedGroups,
    deleteGroupAffiliation,
    deleteGroupInvitation,
    fetchGroupMembers,
} from "../../services/GroupsharingService";
import { 
    fetchSharedContent,
    fetchLikedContent, 
    createLikedContent,
    changeSharedContent,
    deleteLikedContent 
} from "../../services/SharedContentService";

// Index Vor Tabbed View
const SEARCH_VIEW = 0;
const REQUEST_INCOMING_VIEW = 1;
const REQUEST_OUTGOING_VIEW = 2;

class SharingOverviewScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            selectedIndex: SEARCH_VIEW,
            friendSearch: "",
            friends: [],
            strangers: [],
            groupSearch: "",

            own_groups: [],

            unaffiliated_groups: [],
            
            new_group_visible: false,
            new_group_name:"",
            new_group_password:"",

            join_group_name:"",
            join_group_password:"",

            new_member: [],

            group_content: [],
            groups: [],
            group_invitations: [],
            likedContent: [],
        };
    }

    // Setting the index of SegmentedControlTab
    handleIndexChange = index => {
        this.setState({
            ...this.state,
            selectedIndex: index
        });
    };


    /**
     * Redirects the user to SignIn screen if he is not signed in
     */
    componentDidMount() {
        if (this.props.signedIn) {
        }

        this.navFocusListenerSub = this.props.navigation.addListener("focus", this.onScreenFocus.bind(this));
    }

    componentWillUnmount() {
        this.navFocusListenerSub();
    }

    /**
     * Called when the user focuses this screen
     */
    async onScreenFocus() {
        if (notNull(this.props?.route?.params?.scannedCode)) {
        } else {
            this.reload().then();
        }
    }

    async reload() {
        if (!this.props.signedIn) {
            // do not load anything if not signed in
            return;
        }
        this.setState({
            ...this.state,
            loading: true,
        });
        this.getSharingData();
    }

    // immediately fires off a reload
    getSharingData = async () => {
        try {

            // Get Groups, Affiliations and Invitations + Shared Content Stuff
            const responseGroups  = await fetchGroups();
            const responseSharedContents = await fetchSharedContent();
            const responseOwnGroupAffiliations = await fetchAffiliatedGroups();
            const responseOwnGroupInvitations = await fetchGroupInvitationsUser(); 
            const responseOwnLikedContent = await fetchLikedContent();


            groups = responseGroups.data;
            content = responseSharedContents.data;
            //ids of all shared contents that the user has liked to check if it is already liked
            const likedContent = responseOwnLikedContent.data.map(lc => lc.content.id)
            console.log(likedContent);

            
            //newest entries first
            //content = content.reverse();
            group_invitations = responseOwnGroupInvitations.data;

            affiliated_groups = responseOwnGroupAffiliations.data;
            affiliated_groups_ids = affiliated_groups.map(ag => ag.group);
            console.log(affiliated_groups_ids);
            all_group_members = []
            for (let counter = 0; counter < affiliated_groups_ids.length; counter++) {
                const single_group = affiliated_groups_ids[counter];
                const group_members = await fetchGroupMembers(single_group);
                const group_members_ids = group_members.data.map(gm => gm.member)
                
                all_group_members.push(group_members_ids);
              }
            //remove array of array
            const all_group_members_new = (all_group_members.flat())
            const unique_group_members = Array.from(new Set(all_group_members_new))
            const group_content_reversed = content.filter(item => unique_group_members.includes(item.contentAuthor.id));
            //make sure that newest content first
            const group_content = group_content_reversed.reverse();

    

            this.setState({
                ...this.state,
                loading: false,
                error: null,
                groups,
                group_content,
                group_invitations,
                affiliated_groups,
                likedContent,
            });

        } catch (e) {
            console.log("Fail")
            this.setState({
                    error: JSON.stringify(e),
                    loading: false
                }
            );
        }
    };

    onChangeField(fieldName, newValue) {
        this.setState({
            ...this.state,
            [fieldName]: newValue
        });
    }

    async createNewGroup(){
        //create new group and affiliation
        try{
            let newGroup = await createGroup(this.getNewGroupData());
            this.props.showToast("Gruppe erstellt.");
            let adminData = {};
            adminData.admin_status = true;
            await createGroupAffiliation(adminData, newGroup.id);
            this.reload();
            this.props.showToast("Gruppe erstellt und beigetreten.");
            //make sure that group creation form does not get rendered
            this.setState({new_group_visible: false,})
        } catch(e){
            console.log("An error happened: ", e);
            this.props.showToast("Gruppe konnte nicht erstellt/beigetreten werden.");
        }
    }

    async joinGroup(){
        const groups = await fetchGroups();
        let group = groups.data.find(g => g.group_name === this.state.join_group_name);
        if (typeof group == 'undefined'){
            this.props.showToast("Ungültiger Gruppenname");
        }
        else if(group.password !== this.state.join_group_password){
            this.props.showToast("Ungültiges Passwort");
        } 
        else{
            let adminData = {};
            adminData.admin_status = false;
            try{
                await createGroupAffiliation(adminData, group.id)
            } catch(e){
                console.log("An error happened: ", e);
            }
            this.props.showToast("Der Gruppe " + this.state.join_group_name + " beigetreten!");
        }     
    }

    async acceptGroupInvitation(invitation_instance){
        let adminData = {};
        adminData.admin_status = false;
        try{
            await createGroupAffiliation(adminData, invitation_instance.group.id)
            await this.deleteInvitation(invitation_instance)
            this.props.showToast("Einladung angenommen.");
        } catch(e){
            console.log("An error happened: ", e);
        }
    }

    async deleteInvitation(invitation_instance){
        try{
            await deleteGroupInvitation(invitation_instance.group.id ,invitation_instance.id );
            this.props.showToast("Einladung gelöscht.");
            this.reload();
        }catch (e) {
            console.error(e);
            this.setState({
                    error: JSON.stringify(e),
                    loading: false,
                }
            );
        } 
    }

    async leaveGroup(affiliation_instance) {
        try {
            await deleteGroupAffiliation(affiliation_instance.group, affiliation_instance.id);
            this.props.showToast("Gruppe verlassen.");
            this.reload();
        } catch (e) {
            console.error(e);
            this.setState({
                    error: JSON.stringify(e),
                    loading: false,
                }
            );
        }        
    }

    async likePost(post){
        contentData = {};
        contentData.id = post.id;

        contentLikeAmountData = {};
        contentLikeAmountData.likeAmount = post.likeAmount + 1;
        try{
            await createLikedContent(contentData);
            await changeSharedContent(contentLikeAmountData, post.id)
            this.props.showToast("Post geliked.");
            this.reload();
        }catch (e) {
            console.error(e);
            this.setState({
                    error: JSON.stringify(e),
                    loading: false,
                }
            );
        }        
    }

    async unlikePost(post){
        
        contentLikeAmountData = {};
        contentLikeAmountData.likeAmount = post.likeAmount -1;
        try{
            await deleteLikedContent(post.id);
            await changeSharedContent(contentLikeAmountData, post.id)
            this.props.showToast("Like entfernt.");
            this.reload();
        }catch (e) {
            console.error(e);
            this.setState({
                    error: JSON.stringify(e),
                    loading: false,
                }
            );
        }        
    }

    getNewGroupData(){
        let groupData = {}
        groupData.group_name = this.state.new_group_name;
        groupData.password = this.state.new_group_password;
        return groupData;
    }

    getGroupCreatorData(groupData){
        let groupCreatorData = {}
        groupCreatorData.group = groupData;
        groupCreatorData.member = groupData.user;
        groupCreatorData.admin_status = true;
        return groupData;
    }

    getGroupNameFromAffiliation(affiliation_instance){
        let group_instance = this.state.groups.find(element => element.id == affiliation_instance.group);
        return group_instance.group_name;
    }



    // -------------------
    // UI Stuff
    // -------------------

    render() {
        if (this.state.error != null) {
            return (<View style={styles.container}>
                <Text>Sorry, ein Fehler ist aufgetreten</Text>
                <Text>{this.state.error}</Text>
            </View>);
        }

        if (!this.props.signedIn) {
            return (
                <View style={[{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    marginHorizontal: 20
                }, styles.container]}>
                    <Paragraph style={{textAlign: "center"}}>
                        Du musst registriert/angemeldet sein.
                    </Paragraph>
                    <Button style={styles.button} mode="contained"
                            onPress={() => Navigation.navigate("AccountTab", "SignIn")}>Login</Button>
                    <Paragraph style={{marginVertical: 10}}>oder</Paragraph>
                    <Button mode="contained" style={{marginVertical: 20}} accessibilityLabel="Jetzt registrieren"
                            onPress={() => Navigation.navigate("AccountTab", "Infotrailer")}>
                        Jetzt registieren
                    </Button>
                </View>
            );
        }

        let viewPane;
        switch (this.state.selectedIndex) {
            case REQUEST_INCOMING_VIEW:
                viewPane = this.GroupOverviewScreen();
                break;
            case REQUEST_OUTGOING_VIEW:
                viewPane = this.renderRequestScreen();
                break;
            default:
                viewPane = this.contentScreen();
        }


        return (
            <View style={styles.container}>
                <ScrollView
                    keyboardShouldPersistTaps="handled" /* when keyboard is open, allow button press */
                    refreshControl={<RefreshControl refreshing={this.state.loading}
                                                    onRefresh={this.reload.bind(this)}/>}>

                    <SegmentedControlTab
                        values={["Beiträge", "Gruppen", "Beitreten"]}
                        tabStyle={{backgroundColor: Colors.veryLightGray, borderWidth: 1.4, borderColor: Colors.green, elevation: 5}}
                        activeTabStyle={{backgroundColor: Colors.green}}
                        tabTextStyle={{color: Colors.black, padding: 3}}
                        selectedIndex={this.state.selectedIndex}
                        onTabPress={this.handleIndexChange}
                    />
                    {viewPane}
                </ScrollView>
            </View>

        );
    }

    navigateToUser = (user) => {
        Navigation.navigate("FriendsTab", "FriendDetail", {
            friend: user
        });
    };

    navigateToGroup = (groupAffiliationInstance, groupName) => {
        Navigation.navigate("SharingTab", "GroupMembers", {
            groupAffiliation: groupAffiliationInstance,
            groupName : groupName,
            });
    };

    /**
     * Renders a FlatList with the given data as renderItems.
     * The buttons will be displayed on each item.
     *
     * @param data Data to be displayed
     * @param createButtonFunction Function which takes a userId as Argument
     * @returns FlatList
     */

    renderInvitationFlatList(data, createButtonFunction) {
        let emptyInfo= <Caption style={styles.emptyInfo}>
            Keine Gruppeneinladungen erhalten.
          </Caption>;
        return (
            <FlatList
                keyboardShouldPersistTaps="handled"
                data={data}
                ListEmptyComponent={() => emptyInfo}
                refreshing={this.state.loading}
                onRefreash={this.reload.bind(this)}
                renderItem={({item}) => (
                    <TouchableRipple accessibilityLabel={"Einladung - " + item.group.group_name}
                                     style={styles.itemTouchableContainer}
                                     onPress={() =>{} }>
                        <View style={styles.itemContainer}>
                            <Text style={styles.username}>{item.group.group_name}</Text>
                            <View style={styles.buttonContainer}>
                                {createButtonFunction(item)}
                            </View>
                        </View>
                    </TouchableRipple>
                )}
                keyExtractor={item => item.id + ""}
            />
        );
    }

    renderGroupFlatList(data, createButtonFunction) {
        let emptyInfo= <Caption style={styles.emptyInfo}>
            Noch keiner Gruppen beigetreten.
          </Caption>;
        return (
            <FlatList
                keyboardShouldPersistTaps="handled"
                data={data}
                ListEmptyComponent={() => emptyInfo}
                refreshing={this.state.loading}
                onRefreash={this.reload.bind(this)}
                renderItem={({item}) => (
                    <TouchableRipple accessibilityLabel={"Gruppen"}
                                     style={styles.itemTouchableContainer}
                                     onPress={() => this.navigateToGroup(item, this.getGroupNameFromAffiliation(item))}>
                        <View style={styles.itemContainer}>
                            <Text style={styles.username}>{this.getGroupNameFromAffiliation(item)}</Text>
                            <View style={styles.buttonContainer}>
                                {createButtonFunction(item)}
                            </View>
                        </View>
                    </TouchableRipple>
                )}
                keyExtractor={item => item.id + ""}
            />
        );
    }

    likeOrUnlikeButton(content){
        if(this.state.likedContent.includes(content.id)){
            return this.removeLikeButton(content)
        }
        else{
            return this.addLikeButton(content)
        } 
    }
    renderContentFlatList(data) {
        let emptyInfo= <Caption style={styles.emptyInfo}>
        Keine Inhalte vorhanden. {"\n"} 
        Tritt einer Gruppe bei für geteilte Inhalte.  {"\n"} 
      </Caption>;
        return (
            <FlatList
                keyboardShouldPersistTaps="handled"
                data={data}
                ListEmptyComponent={() => emptyInfo}
                refreshing={this.state.loading}
                onRefreash={this.reload.bind(this)}
                renderItem={({item}) => (
                    <TouchableRipple accessibilityLabel={"Geteilte Inhalte"}
                                     style={styles.itemTouchableContainerForSharedContent}>
                        <View>
                            <Paragraph style={styles.username}>{item.contentAuthor.username} hat  <Text style={styles.underlinedText}>{item.contentType} {item.contentText}</Text> geteilt.</Paragraph>
                            <Paragraph style={styles.username}>
                                {this.likeOrUnlikeButton(item)}
                                {item.likeAmount}
                            </Paragraph>
                        </View>
                    </TouchableRipple>
                )}
                keyExtractor={item => item.id + ""}
            />
        );
    }

    createGroupForms() {
        if (this.state.new_group_visible == true){
            return (
                <View>
                <Headline style={styles.heading}>Gruppe erstellen</Headline>
                <TextInput
                    placeholder="Gruppenname"
                    keyboardType="default"
                    value={this.state.new_group_name}
                    style={styles.textinput}
                    onChangeText={val => this.onChangeField("new_group_name", val)}
                />
                <TextInput
                    placeholder="Passwort"
                    keyboardType="default"
                    value={this.state.new_group_password}
                    style={styles.textinput}
                    onChangeText={val => this.onChangeField("new_group_password", val)}
                />
                <Button 
                    style={styles.button}
                    onPress={() => this.createNewGroup()}>
                    Erstellen
                </Button>
               </View>
            );
        } else {
            return (
                <View  style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                    <IconButton small
                    icon="plus-circle"
                    color={Colors.green}
                    accessibilityLabel={"Gruppe erstellen - " + content}
                    onPress={() => this.setState({new_group_visible: true})}
                    key={content + "createGroup"}
                    />
                </View>
            )
        }
    }

    // -------------------
    // Buttons
    // -------------------

    addLikeButton = (content) => (
        <IconButton small
                    icon="thumb-up-outline"
                    color={Colors.green}
                    accessibilityLabel={"Content liken - " + content}
                    onPress={() => this.likePost(content)}
                    key={content + "likeContent"}
        />
    );

    removeLikeButton = (content) => (
        <IconButton small
                    icon="thumb-up"
                    color={Colors.green}
                    accessibilityLabel={"Content unliken - " + content}
                    onPress={() => this.unlikePost(content)}
                    key={content + "unlikeContent"}
        />
    );

    leaveGroupButton = (groupAffiliationId) => (
        <IconButton small
                    icon="exit-run"
                    color={Colors.red}
                    accessibilityLabel={"Gruppe verlassen " + groupAffiliationId}
                    onPress={() => this.leaveGroup(groupAffiliationId)}
                    key={groupAffiliationId + "leaveGroup"}
        />
    );

    acceptInvitationButton = (groupInvitationInstance) => (
        <IconButton small
                    icon="checkbox-marked"
                    color={Colors.green}
                    accessibilityLabel={"Gruppeneinladung annehmen " + groupInvitationInstance.id}
                    onPress={() => this.acceptGroupInvitation(groupInvitationInstance)}
                    key={groupInvitationInstance + "acceptInvitation"}
        />
    );

    rejectInvitationButton = (groupInvitationInstance) => (
        <IconButton small
                    icon="minus-box"
                    color={Colors.red}
                    accessibilityLabel={"Gruppeneinladung ablehnen " + groupInvitationInstance.id}
                    onPress={() => this.deleteInvitation(groupInvitationInstance)}
                    key={groupInvitationInstance.id + "rejectGroupInvitation"}
        />
    );

    buttonsForStrangerListItem = (userId) => {
        const arr = [];
        const type = this.state.userTypeMap.get(userId);

        if (type === UT_FRIEND_REQUEST_SENT) {
            arr.push(this.deleteFriendRequestButton);
        } else if (type === UT_FRIEND_REQUEST_INCOMING) {
            arr.push(this.confirmFriendRequestButton);
            arr.push(this.deleteFriendRequestButton);
        } else {
            arr.push(this.addFriendButton);
        }

        return (
            arr.map((buttonFn) => {
                return buttonFn(userId);
            }));
    };

    // -------------------
    // Screens
    // -------------------

    contentScreen() {
        const sharedContentData = this.state.group_content;
        const likeButton = this.addLikeButton


        let contentList = <View>
            <Headline style={styles.heading}>Geteilte Inhalte</Headline>
            {this.renderContentFlatList(sharedContentData, likeButton)}
            </View>;
        return (
            <View>
                {contentList}
            </View>
        );
    }

    GroupOverviewScreen() {
        const groupData = this.state.groups;
        const groupData2 = groupData.map(g => g.group_name)

        

        const arr = [this.confirmFriendRequestButton, this.deleteFriendRequestButton];
        const incomingRequestButtons = (userId) => {
            return arr.map(buttonFn => {
                return buttonFn(userId);
            });
        };
        
        const ownGroupsData = this.state.affiliated_groups;
        const leaveGroupButton = this.leaveGroupButton

        let ownGroupList = <View>
            <Headline style={styles.heading}>Deine Gruppen</Headline>
            {this.renderGroupFlatList(ownGroupsData, leaveGroupButton)}
            </View>;

        return (
            <View>
                {this.createGroupForms()}
                {ownGroupList}
            </View>
        );
    }

    renderRequestScreen() {
                
        const groupInvitationsData = this.state.group_invitations;
        const arr = [this.acceptInvitationButton, this.rejectInvitationButton];

        const groupInvitationButtons = (invitation_id) => {
            return arr.map(buttonFn => {
                return buttonFn(invitation_id);
            });
        };

        let invitationList = <View>
            <Headline style={styles.heading}>Erhaltene Anfragen</Headline>
            {this.renderInvitationFlatList(groupInvitationsData, groupInvitationButtons)}
            </View>;
        return (
            <View>
                <Headline style={styles.heading}>Gruppe beitreten</Headline>
                <TextInput
                    placeholder="Gruppenname"
                    keyboardType="default"
                    style={styles.textinput}
                    onChangeText={val => this.onChangeField("join_group_name", val)}
                />
      
                <TextInput
                    placeholder="Passwort"
                    keyboardType="default"
                    style={styles.textinput}
                    onChangeText={val => this.onChangeField("join_group_password", val)}
                />
                <Button style={styles.button} onPress={() => this.joinGroup()}>Beitreten</Button>
                <Divider />
                {invitationList}

                

            </View>
        );
    }

}

const {width, height} = Dimensions.get("window");
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    searchField: {
        marginTop: 20,
        marginLeft: 5,
        maxWidth: width,
        color: "lightgrey",
    },
    graySearchField: {
        marginTop: 20,
        marginLeft: 5,
        maxWidth: width,
        color: "green",
    },
    button: {
        marginVertical: 8,
        minWidth: "50%",
    },
    itemTouchableContainer: {
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 10,
        borderBottomColor: "lightgrey",
        borderTopColor: "lightgrey",
        borderBottomWidth: 1,
        borderTopWidth: 1,
        height: 80,
        display: "flex",
        justifyContent: "center"
    },
    itemTouchableContainerForSharedContent: {
        marginTop: 5,
        marginBottom: 5,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 10,
        borderTopColor: "lightgrey",
        borderTopWidth: 1,
        display: "flex",
        justifyContent: "center"
    },
    itemContainer: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },
    buttonContainer: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },
    heading: {
        marginTop: 30,
        marginBottom: 10,
        textAlign: "center",
    },
    username: {
        fontSize: 16,
    },
    searchView: {
        display: "flex",
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "flex-end"
    },
    textinput: {
        color: "white",
        marginLeft: 5,
        maxWidth: width
    },
    emptyInfo: {
        marginVertical: 10,
        marginHorizontal: 8,
        textAlign: "center"
    },
    underlinedText: {
        textDecorationLine: "underline"
    },
    scannerToggler: {}
});

const mapStateToProps = state => {
    return {
        signedIn: !state.account.account.pseudonymous,
        username: state.account.account.username
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({
    showToast,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SharingOverviewScreen);
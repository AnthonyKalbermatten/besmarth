import React from "react";
import {createEmptyTestStore, createNetworkMock, renderScreenForTest, waitForReduxAction} from "../../test/test-utils";
import HomeScreen from "../HomeScreen";

const mock = createNetworkMock();

const topic = {
  "imageUrl": "/media/images/topic-levels/level02.png",
  "level": 2,
  "levelDescription": "Klimasupporter",
  "score": 0,
  "scoreNextLevel": 5,
  "scorePrevLevel": 1,
};

// mock MapItem to prevent weird test failure
jest.mock('../../components/MapItem.js', () => 'MapItem');

describe("HomeScreen", () => {
  it(`renders`, async () => {
    mock.reset();
    mock.onGet("/topics/").reply(200, {
        "count": 4,
        "next": null,
        "previous": null,
        "results": [
          {
            "id": 1,
            "internal_id": 0,
            "topic_name": "Wald",
            "image_level1": "http://localhost:8000/media/images/topics/thumb-1920-80484.jpg",},
          {
            "id": 2,
            "internal_id": 2,
            "topic_name": "Ozean",
            "image_level1": "http://localhost:8000/media/images/topics/ocean_TTIoVd0.jpg",
          },
          {
            "id": 3,
            "internal_id": 1,
            "topic_name": "Abfall",
            "image_level1": "http://localhost:8000/media/images/topics/evgeny-karchevsky-k1tUxfs8JYY-unsplash_C7ygJLc.jpg",
          },
          {
            "id": 4,
            "internal_id": 3,
            "topic_name": "Essen",
            "image_level1": "http://localhost:8000/media/images/topics/megan-thomas-xMh_ww8HN_Q-unsplash_kyFmbg4.jpg"
          }
        ]
      }
    );
    mock.onGet("/topics/1/progress").reply(200, [topic]);
    mock.onGet("/topics/2/progress").reply(200, [topic]);
    mock.onGet("/topics/3/progress").reply(200, [topic]);
    mock.onGet("/topics/4/progress").reply(200, [topic]);

    // I use renderForTest here because
    const {asJSON} = renderScreenForTest(HomeScreen, createEmptyTestStore());
    await waitForReduxAction("RECEIVED_TOPICS");

    expect(asJSON()).toMatchSnapshot();
  });
});

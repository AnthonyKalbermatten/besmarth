import RemoteStorage from "./RemoteStorage";

const GROUP_API_PATH = "/groups/";
const GROUP_INVITATION_API_PATH = "/groups/groupInvitations/";
const GROUP_AFFILIATION_PATH = "/groups/groupAffiliations/";


//functions for groups

export async function createGroup(groupData) {
  //return await RemoteStorage.post(GROUP_API_PATH, groupData);
  const response = await RemoteStorage.post(GROUP_API_PATH, groupData);
  const group = response.data;
  return group;
}

export async function fetchGroups() {
  return await RemoteStorage.get(GROUP_API_PATH);
}

export async function deleteGroup(groupId) {
  return await RemoteStorage.delete(GROUP_API_PATH + groupId);
}


//functions for group affiliations

export async function fetchAffiliatedGroups() {
  return await RemoteStorage.get(GROUP_AFFILIATION_PATH);
}

export async function fetchGroupMembers(groupId) {
  return await RemoteStorage.get(GROUP_API_PATH + `${groupId}/groupAffiliations/`);
}

export async function createGroupAffiliation(AdminStatusData, groupId) {
  return await RemoteStorage.post(GROUP_API_PATH + `${groupId}/groupAffiliations/`, AdminStatusData);
}

export async function deleteGroupAffiliation(groupId, groupInstanceId) {
  return await RemoteStorage.delete(GROUP_API_PATH + `${groupId}/groupAffiliations/${groupInstanceId}`);
}

//functions for group invitations
export async function fetchGroupInvitationsUser() {
  return await RemoteStorage.get(GROUP_INVITATION_API_PATH);
}

export async function fetchGroupInvitationsGroup() {
  return await RemoteStorage.get(GROUP_API_PATH + `${groupId}/groupInvitations/`);
}


export async function createGroupInvitation(receiverData, groupId) {
  return await RemoteStorage.post(GROUP_API_PATH + `${groupId}/groupInvitations/`, receiverData);
}

export async function deleteGroupInvitation(groupId, groupInvitationId) {
  return await RemoteStorage.delete(GROUP_API_PATH + `${groupId}/groupInvitations/${groupInvitationId}`);
}




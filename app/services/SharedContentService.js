import RemoteStorage from "./RemoteStorage";

const SHAREDCONTENT_API_PATH = "/sharing/";
const LIKEDCONTENT_API_PATH = "/sharing/likes/";

//SharedContent get and post
export async function createSharedContent(contentData) {
  return await RemoteStorage.post(SHAREDCONTENT_API_PATH, contentData);
}

export async function fetchSharedContent() {
  return await RemoteStorage.get(SHAREDCONTENT_API_PATH);
}

export async function changeSharedContent(newSharedData, contentId) {
  return await RemoteStorage.patch(SHAREDCONTENT_API_PATH + contentId, newSharedData);
}

//LikedContent get and post
export async function createLikedContent(contentIdData) {
    return await RemoteStorage.post(LIKEDCONTENT_API_PATH, contentIdData);
}

export async function fetchLikedContent() {
    return await RemoteStorage.get(LIKEDCONTENT_API_PATH);
}


export async function deleteLikedContent(contentId) {
    return await RemoteStorage.delete(LIKEDCONTENT_API_PATH + contentId);
}



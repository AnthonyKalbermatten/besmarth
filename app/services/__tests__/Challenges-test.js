import {CHALLENGES_CACHE_KEY, fetchChallenges, getNumberOfCompletedChallenges} from "../Challenges";
import LocalStorage from "../LocalStorage";
import {isPhoneOnline} from "../phone-utils";

jest.unmock("../phone-utils.js");
jest.mock("../phone-utils.js", () => {
    return {
      isPhoneOnline: () => Promise.resolve(false),
      isConnectedToWifi: () => Promise.resolve(false),
    };
  }
);

const challenge = {
  id: 1,
  title: "challenge Title",
  image: "http://test.local/image/url",
  color: "#abcdef",
  periodicity: "daily",
  duration: 4,
  topic: 1
};

const participation1 = {
  id: 7,
  challenge: challenge,
  progress: [
    {
      id: 1,
      create_time: "2019-11-29T09:37:35.725283+01:00",
      participation: 1,
      mark_deleted: false
    },
    {
      id: 2,
      create_time: "2019-11-30T09:37:35.725283+01:00",
      participation: 1,
      mark_deleted: false
    },
    {
      id: 3,
      create_time: "2019-12-01T09:37:35.725283+01:00",
      participation: 1,
      mark_deleted: false
    },
    {
      id: 4,
      create_time: "2019-12-02T09:37:35.725283+01:00",
      participation: 1,
      mark_deleted: false
    }
  ],
  shared: false,
  mark_deleted: false,
  progress_loggable: true
};

const participation2 = {
  id: 8,
  challenge: challenge,
  progress: [
    {
      id: 1,
      create_time: "2019-11-29T09:37:35.725283+01:00",
      participation: 1,
      mark_deleted: false
    },
    {
      id: 2,
      create_time: "2019-11-30T09:37:35.725283+01:00",
      participation: 1,
      mark_deleted: false
    },
    {
      id: 3,
      create_time: "2019-12-01T09:37:35.725283+01:00",
      participation: 1,
      mark_deleted: false
    },
    {
      id: 4,
      create_time: "2019-12-02T09:37:35.725283+01:00",
      participation: 1,
      mark_deleted: false
    },
    {
      id: 5,
      create_time: "2019-12-03T09:37:35.725283+01:00",
      participation: 1,
      mark_deleted: false
    },
    {
      id: 6,
      create_time: "2019-12-04T09:37:35.725283+01:00",
      participation: 1,
      mark_deleted: false
    },
    {
      id: 7,
      create_time: "2019-12-05T09:37:35.725283+01:00",
      participation: 1,
      mark_deleted: false
    },
    {
      id: 8,
      create_time: "2019-12-06T09:37:35.725283+01:00",
      participation: 1,
      mark_deleted: false
    },
    {
      id: 9,
      create_time: "2019-12-07T09:37:35.725283+01:00",
      participation: 1,
      mark_deleted: false
    },
  ],
  shared: false,
  mark_deleted: false,
  progress_loggable: true
};
const participation3 = {
  id: 7,
  challenge: challenge,
  progress: [
    {
      id: 1,
      create_time: "2019-11-29T09:37:35.725283+01:00",
      participation: 1,
      mark_deleted: false
    },
    {
      id: 2,
      create_time: "2019-11-30T09:37:35.725283+01:00",
      participation: 1,
      mark_deleted: false
    }
  ],
  shared: false,
  mark_deleted: false,
  progress_loggable: true
};


describe(`Challenges`, () => {
  it(`calculate number of completed challenges`, () => {
    let returnValue1 = getNumberOfCompletedChallenges(participation1, challenge);
    let returnValue2 = getNumberOfCompletedChallenges(participation2, challenge);
    let returnValue3 = getNumberOfCompletedChallenges(participation3, challenge);
    expect(returnValue1).toBe(1);
    expect(returnValue2).toBe(2);
    expect(returnValue3).toBe(0);
  });

  it(`fetchChallenges when phone is offline throws error`, async () => {
    jest.mock("../phone-utils", () => {
        return {
          isPhoneOnline: false,
          isConnectedToWifi: false,
        };
      }
    );

    try {
      const challenges = await fetchChallenges(1);
      fail("Error should be thrown if phone is offline and no challenges in cache");
    } catch (e) {
      expect("Error is thrown when phone is offline").toBe("Error is thrown when phone is offline");
    }
  });

  it(`fetchChallenges when phone is offline takes challenges from cache`, async () => {
    jest.mock("../phone-utils", () => {
        return {
          isPhoneOnline: false,
          isConnectedToWifi: false,
        };
      }
    );
    await LocalStorage.setCacheItem(CHALLENGES_CACHE_KEY, [challenge]);
    const challenges = await fetchChallenges(1);
    expect(challenges.length).toBe(1);
    expect(challenges[0].title).toBe("challenge Title");
  });

  it(`fetchChallenges when phone is offline takes challenges from cache still filters after topicId`, async () => {
    await LocalStorage.setCacheItem(CHALLENGES_CACHE_KEY, [challenge]);
    const challenges = await fetchChallenges(2);
    expect(challenges.length).toBe(0);
  });
});

import {DefaultTheme} from 'react-native-paper';

export default {
  ...DefaultTheme,
  roundness: 10,
  colors: {
    ...DefaultTheme.colors,
    primary: '#006B2A',
    accent: '#006B2A',
    background: 'white'
  },

};

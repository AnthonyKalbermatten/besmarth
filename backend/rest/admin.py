from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from import_export.fields import Field

from .models import Tip, TipSchedule, Challenge, Topic, Account, Reward, AchievedReward, TopicLevel, Diary, Hypothesis, \
    DailyMonitoring, WeeklyMonitoring, FinalMonitoring, Post, Factor, ChallengeParticipation, UserChallenge, \
    SelfCommitment, Group, GroupAffiliation, GroupInvitation, DailyHelpText, SharedContent, LikedContent 

ID_DIARY_STRING = 'ID Tagebuch'

# Manage Account
admin.site.register(Account)

# Manage Group
admin.site.register(Group)

# Manage Group Affiliations
admin.site.register(GroupAffiliation)

# Manage Group Invitation
admin.site.register(GroupInvitation)


# Manage topics
admin.site.register(Topic)

# Manage Challenges
admin.site.register(Challenge)

# Manage Self Commitments
admin.site.register(SelfCommitment)

# Manage daily tips
admin.site.register(Tip)


# admin.site.register(TipSchedule)

# Manage diaries - for export
class DiaryResource(resources.ModelResource):
    id = Field(attribute='id', column_name=ID_DIARY_STRING)
    user__username = Field(attribute='user__username', column_name='Username')
    user__first_name = Field(attribute='user__first_name', column_name='Vorname')
    user__last_name = Field(attribute='user__last_name', column_name='Nachname')
    user__email = Field(attribute='user__email', column_name='E-Mail')
    user__gender = Field(attribute='gender', column_name='Geschlecht')
    user__age = Field(attribute='age', column_name='Alter')
    selfCommitment = Field(attribute='selfCommitment', column_name='Self-Commitment')
    selfCommitment_reason = Field(attribute='selfCommitment_reason', column_name='Begründung für Self-Commitment')
    pushNotification_time = Field(attribute='pushNotification_time', column_name='Zeitpunkt der Push-Benachrichtigung')
    challenge__title = Field(attribute='challenge__title', column_name='Challenge Titel')
    challenge__description = Field(attribute='challenge__description', column_name='Challenge Beschreibung')
    challenge__duration = Field(attribute='challenge__duration', column_name='Challenge Dauer')
    challenge__periodicity = Field(attribute='challenge__periodicity', column_name='Challenge Häufigkeit')
    challenge__category = Field(attribute='challenge__category', column_name='Challenge Kategorie')
    challenge__topic__topic_name = Field(attribute='challenge__topic__topic_name',
                                         column_name='Topic Name der Challenge'),
    date_created = Field(attribute='date_created', column_name='Created Date')

    class Meta:
        model = Diary
        fields = (
            'id',
            'date_created',
            'user__username',
            'user__first_name',
            'user__last_name',
            'user__email',
            'user__gender',
            'user__age',
            'selfCommitment',
            'selfCommitment_reason',
            'pushNotification_time',
            'challenge__title',
            'challenge__description',
            'challenge__duration',
            'challenge__periodicity',
            'challenge__category',
            'challenge__topic__topic_name'
        )
        export_order = (
            'id',
            'date_created',
            'user__username',
            'user__first_name',
            'user__last_name',
            'user__email',
            'user__gender',
            'user__age',
            'selfCommitment',
            'selfCommitment_reason',
            'pushNotification_time',
            'challenge__title',
            'challenge__description',
            'challenge__duration',
            'challenge__periodicity',
            'challenge__category',
            'challenge__topic__topic_name'
        )


class DiaryAdmin(ImportExportModelAdmin):
    resource_class = DiaryResource


admin.site.register(Diary, DiaryAdmin)


# Manage hypothesis - for export
class HypothesisResource(resources.ModelResource):
    diary__id = Field(attribute='diary__id', column_name=ID_DIARY_STRING)
    startHypothesis = Field(attribute='startHypothesis', column_name='Hypothesis bei Beginn der Challenge')
    endHypothesis = Field(attribute='endHypothesis', column_name='Hypothesis beim Ende der Challenge')
    difficulty = Field(attribute='difficulty', column_name='Wie schwer ist das Self-Commitment?')
    limitation = Field(attribute='limitation', column_name='Wie stark limitiert mich das Self-Commitment?')
    environment = Field(attribute='environment', column_name='Wie reagiert mein Umfeld auf das Self-Commitment?')
    contribution = Field(attribute='contribution', column_name='Wie viel kann dieses Self-Commitment beitragen?')
    burden = Field(attribute='burden', column_name='Wie stark wird mich dieses Self-Commitment belasten?')
    development = Field(attribute='development', column_name='Wie stark trägt das gewählte Self-Commitment zu einer nachhaltigen Entwicklung bei?')
    date_created = Field(attribute='date_created', column_name='Created Date')

    class Meta:
        model = Hypothesis
        fields = (
            'diary__id',
            'date_created',
            'startHypothesis',
            'endHypothesis',
            'difficulty',
            'limitation',
            'environment',
            'contribution',
            'burden',
            'development'
        )
        export_order = (
            'diary__id',
            'date_created',
            'startHypothesis',
            'endHypothesis',
            'difficulty',
            'limitation',
            'environment',
            'contribution',
            'burden',
            'development'
        )


class HypothesisAdmin(ImportExportModelAdmin):
    resource_class = HypothesisResource


admin.site.register(Hypothesis, HypothesisAdmin)


# Manage daily monitorings - for export
class DailyMonitoringResource(resources.ModelResource):
    diary__id = Field(attribute='diary__id', column_name=ID_DIARY_STRING)
    wellbeing = Field(attribute='wellbeing', column_name='Wie fühlst du dich heute?')
    done = Field(attribute='done', column_name='Wurde das Self-Commitment heute eingelöst?')
    timestamp = Field(attribute='timestamp', column_name='Datum der täglichen Beurteilung')
    date_created = Field(attribute='date_created', column_name='Created Date')

    class Meta:
        model = DailyMonitoring
        fields = (
            'diary__id',
            'date_created',
            'wellbeing',
            'done',
            'timestamp'
        )
        export_order = (
            'diary__id',
            'date_created',
            'wellbeing',
            'done',
            'timestamp'
        )


class DailyMonitoringAdmin(ImportExportModelAdmin):
    resource_class = DailyMonitoringResource


admin.site.register(DailyMonitoring, DailyMonitoringAdmin)


# Manage weekly monitorings - for export
class WeeklyMonitoringResource(resources.ModelResource):
    diary__id = Field(attribute='diary__id', column_name=ID_DIARY_STRING)
    week = Field(attribute='week', column_name='Woche')
    self_action = Field(attribute='self_action', column_name='Massnahme die man selbst umsetzen kann')
    socialEnvironment_action = Field(attribute='socialEnvironment_action',
                                     column_name='Massnahme für eine Veränderung im sozialen Umfeld')
    political_action = Field(attribute='political_action', column_name='Massnahme auf politischer Ebene')
    producer_action = Field(attribute='producer_action', column_name='Massnahme auf der Ebene von Produktherstellern')
    date_created = Field(attribute='date_created', column_name='Created Date')

    class Meta:
        model = WeeklyMonitoring
        fields = (
            'diary__id',
            'date_created',
            'week',
            'self_action',
            'socialEnvironment_action',
            'political_action',
            'producer_action'
        )
        export_order = (
            'diary__id',
            'date_created',
            'week',
            'self_action',
            'socialEnvironment_action',
            'political_action',
            'producer_action'
        )


class WeeklyMonitoringAdmin(ImportExportModelAdmin):
    resource_class = WeeklyMonitoringResource


admin.site.register(WeeklyMonitoring, WeeklyMonitoringAdmin)


# Manage posts - for export
class PostResource(resources.ModelResource):
    diary__id = Field(attribute='diary__id', column_name=ID_DIARY_STRING)
    text = Field(attribute='text', column_name='Inhalt des Posts')
    positive = Field(attribute='positive', column_name='Ist es ein positiver Post?')
    week = Field(attribute='week', column_name='Zugehörige Woche des Posts')
    date_created = Field(attribute='date_created', column_name='Created Date')

    class Meta:
        model = Post
        fields = (
            'diary__id',
            'date_created',
            'text',
            'positive',
            'week'
        )
        export_order = (
            'diary__id',
            'date_created',
            'text',
            'positive',
            'week'
        )


class PostAdmin(ImportExportModelAdmin):
    resource_class = PostResource


admin.site.register(Post, PostAdmin)


# Manage factors - for export
class FactorResource(resources.ModelResource):
    diary__id = Field(attribute='diary__id', column_name=ID_DIARY_STRING)
    text = Field(attribute='text', column_name='Inhalt des Faktors')
    positive = Field(attribute='positive', column_name='Ist es ein positiver Faktor?')
    finalFactor = Field(attribute='finalFactor', column_name='Ist der Faktor ein finaler Faktor vom Schlussbericht?')
    week = Field(attribute='week', column_name='Zugehörige Woche des Faktors')
    date_created = Field(attribute='date_created', column_name='Created Date')

    class Meta:
        model = Factor
        fields = (
            'diary__id',
            'date_created',
            'text',
            'positive',
            'finalFactor',
            'week'
        )
        export_order = (
            'diary__id',
            'date_created',
            'text',
            'positive',
            'finalFactor',
            'week'
        )


class FactorAdmin(ImportExportModelAdmin):
    resource_class = FactorResource


admin.site.register(Factor, FactorAdmin)

# Manage rewards - currently deactivated
# admin.site.register(Reward)
# admin.site.register(AchievedReward)

# Manage levels - currently deactivated
# admin.site.register(TopicLevel)

# FOR DEVELOPMENT ONLY
admin.site.register(FinalMonitoring)
# admin.site.register(ChallengeParticipation)
admin.site.register(UserChallenge)

#in order for the customer to create the help texts
admin.site.register(DailyHelpText)


# Manage shared content - for export
class SharedContentResource(resources.ModelResource):
    id = Field(attribute='id', column_name="Beitrags ID")
    contentText = Field(attribute='contentText', column_name='Beitragstext')
    contentAuthor = Field(attribute='contentAuthor', column_name='Beitragsverfasser')
    contentType = Field(attribute='contentType', column_name='Beitragstyp')
    likeAmount = Field(attribute='likeAmount', column_name='Anzahl Likes')
    

    class Meta:
        model = Diary
        fields = (
            'id',
            'contentText',
            'contentAuthor',
            'contentType',
            'likeAmount',
        )
        export_order = (
            'id',
            'contentText',
            'contentAuthor',
            'contentType',
            'likeAmount',
        )


class SharedContentAdmin(ImportExportModelAdmin):
    resource_class = SharedContentResource


admin.site.register(SharedContent, SharedContentAdmin)

admin.site.register(LikedContent)
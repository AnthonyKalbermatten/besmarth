from django.contrib.auth.hashers import make_password
from rest_framework import serializers

from .models import Tip, Challenge, Account, Group, GroupAffiliation, GroupInvitation, Topic, ChallengeParticipation, ChallengeProgress, FriendRequest, Reward, \
    AchievedReward, Friendship, Diary, Hypothesis, DailyMonitoring, WeeklyMonitoring, FinalMonitoring, Post, Factor, UserChallenge, SelfCommitment, DailyHelpText, SharedContent, LikedContent

class CreateAccountSerializer(serializers.Serializer):
    """ Serializer for POST /account  most fields from account are not needed, therefore this is not a ModelSerializer"""
    username = serializers.CharField(required=False)
    password = serializers.CharField(required=False)
    pseudonymous = serializers.BooleanField(default=False, required=False)
    data_usage = serializers.BooleanField(default=False, required=False)
    gender = serializers.CharField(required=False)
    age = serializers.CharField(required=False)

    def validate(self, attrs):
        pseudonymous = attrs.get('pseudonymous')
        username = attrs.get('username')
        password = attrs.get('password')

        if pseudonymous or (username and password):
            pass
        else:
            raise serializers.ValidationError('Must include either "pseudonymous" or "username" and "password".')
        return attrs



class AccountSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'username', 'email', 'password', 'first_name', 'pseudonymous', 'data_usage', 'gender', 'age']

    @staticmethod
    def validate_password(value: str) -> str:
        return make_password(value)

    def to_representation(self, obj):
        ret = super(AccountSerializer, self).to_representation(obj)
        # do not output password
        ret.pop('password')
        return ret

#all fields for group creation are needed, this a ModelSerializer will suffice
class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['id', 'group_name', 'password']


    @staticmethod
    def validate_password(value: str) -> str:
        return make_password(value)

class GroupAffiliationSerializer(serializers.ModelSerializer):
    group = GroupSerializer
    member = AccountSerializer

    class Meta:
        model = GroupAffiliation
        fields = ['id', 'group', 'member', 'admin_status']


class GroupInvitationSerializer(serializers.ModelSerializer):
    sender = AccountSerializer()
    receiver = AccountSerializer()
    group = GroupSerializer()

    class Meta:
        model = GroupInvitation
        fields = ['id', 'receiver', 'sender', 'group']

class TipSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tip
        fields = ['id', 'title', 'description', 'image']


class TopicSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Topic
        fields = ['id', 'internal_id', 'topic_name', 'topic_description', 'image_level1', 'image_level2', 'image_level3',
                  'image_level4', 'image_level5']


class SelfCommitmentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SelfCommitment
        fields = ['self_commitment1', 'self_commitment2', 'self_commitment3', 'self_commitment4', 'self_commitment5']


class ChallengeSerializer(serializers.HyperlinkedModelSerializer):
    topic = serializers.SlugRelatedField(
        many=False,
        slug_field='topic_name',
        queryset=Topic.objects.all()
    )

    selfCommitment = SelfCommitmentSerializer(many=False)

    class Meta:
        model = Challenge
        fields = ['id', 'title', 'description', 'icon', 'image',
                  'duration', 'color', 'difficulty', 'periodicity', 'topic',
                  'category', 'selfCommitment']

    def to_representation(self, obj):
        ret = super(ChallengeSerializer, self).to_representation(obj)
        ret['image'] = self.context['request'].build_absolute_uri(ret['image'])
        ret['icon'] = self.context['request'].build_absolute_uri(ret['icon'])
        return ret

    def create(self, validated_data):
        image = "images/challenges/standard-image-challenge.jpg"
        selfcommitment_data = validated_data.pop('selfCommitment')
        selfcommitment = SelfCommitment.objects.create(**selfcommitment_data)
        challenge = Challenge.objects.create(**validated_data, image=image, selfCommitment=selfcommitment)
        return challenge


class UserChallengeSerializer(serializers.HyperlinkedModelSerializer):
    topic = serializers.SlugRelatedField(
        many=False,
        slug_field='topic_name',
        queryset=Topic.objects.all()
    )

    selfCommitment = SelfCommitmentSerializer(many=False)
    user = AccountSerializer(many=False, read_only=True)

    class Meta:
        model = UserChallenge
        fields = ['id', 'internal_id', 'title', 'description', 'icon', 'image',
                  'duration', 'color', 'difficulty', 'periodicity', 'topic',
                  'category', 'selfCommitment', 'user']

    def to_representation(self, obj):
        ret = super(UserChallengeSerializer, self).to_representation(obj)
        ret['image'] = self.context['request'].build_absolute_uri(ret['image'])
        ret['icon'] = self.context['request'].build_absolute_uri(ret['icon'])
        return ret

    def create(self, validated_data):
        challenge = UserChallenge.objects.create(**validated_data)
        return challenge


class ChallengeProgressSerializer(serializers.HyperlinkedModelSerializer):
    participation = serializers.PrimaryKeyRelatedField(many=False, read_only=True)

    class Meta:
        model = ChallengeProgress
        fields = ['id', 'create_time', 'participation', 'mark_deleted']


class ChallengeParticipationSerializer(serializers.HyperlinkedModelSerializer):
    challenge = UserChallengeSerializer(many=False, read_only=True)
    user = AccountSerializer(many=False, read_only=True)
    progress = ChallengeProgressSerializer(many=True)
    progress_loggable = serializers.ReadOnlyField()

    class Meta:
        model = ChallengeParticipation
        fields = ['id', 'joined_time', 'challenge', 'user', 'progress', 'shared', 'mark_deleted', 'progress_loggable']


class FriendshipRequestSerializer(serializers.ModelSerializer):
    receiver = AccountSerializer()
    sender = AccountSerializer()

    class Meta:
        model = FriendRequest
        fields = ['id', 'receiver', 'sender', 'date']


class RewardSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Reward
        fields = ['id', 'type', 'color', 'points', 'condition']


class AchievedRewardSerializer(serializers.HyperlinkedModelSerializer):
    reward = RewardSerializer()

    class Meta:
        model = AchievedReward
        fields = ['reward']


class AccountExportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'username', 'email', 'first_name', 'last_name', 'pseudonymous', 'is_active', 'date_joined',
                  'last_login']


class MinimalAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'username', 'first_name']


class MinimalFriendshipSerializer(serializers.ModelSerializer):
    receiver = MinimalAccountSerializer()
    sender = MinimalAccountSerializer()

    class Meta:
        model = Friendship
        fields = ['receiver', 'sender', 'date']


class HypothesisSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Hypothesis
        fields = ['id', 'startHypothesis', 'endHypothesis', 'difficulty', 'limitation', 'environment', 'contribution', 'burden','development','date_created']


class DailyMonitoringSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DailyMonitoring
        fields = ['id', 'wellbeing', 'done', 'timestamp', 'date_created']


class WeeklyMonitoringSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = WeeklyMonitoring
        fields = ['id', 'week', 'self_action', 'socialEnvironment_action', 'political_action', 'producer_action', 'date_created']


class FinalMonitoringSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FinalMonitoring
        fields = ['id', 'date_created']


class PostSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Post
        fields = ['id', 'text', 'positive', 'week', 'date_created']


class FactorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Factor
        fields = ['id', 'text', 'positive', 'finalFactor', 'week', 'date_created']


class DiarySerializer(serializers.HyperlinkedModelSerializer):
    user = AccountSerializer(many=False, required=False, help_text="Linked user")
    challenge = UserChallengeSerializer(many=False, required=False, help_text="Linked challenge")
    hypothesises = HypothesisSerializer(many=True, required=False, allow_null=False, help_text="Linked hypothesises")
    daily_monitorings = DailyMonitoringSerializer(many=True, required=False, help_text="Linked daily monitorings")
    weekly_monitorings = WeeklyMonitoringSerializer(many=True, required=False, help_text="Linked weekly monitorings")
    final_monitoring = FinalMonitoringSerializer(many=True, required=False, help_text="Linked final monitoring")
    posts = PostSerializer(many=True, required=False, help_text="Linked posts")
    factors = FactorSerializer(many=True, required=False, help_text="Linked factors")

    class Meta:
        model = Diary
        fields = ['id', 'user', 'challenge', 'selfCommitment', 'selfCommitment_reason', 'pushNotification_time', 'hypothesises', 'daily_monitorings', 'weekly_monitorings', 'final_monitoring', 'posts', 'factors', 'date_created']

class DailyHelpTextSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = DailyHelpText
        fields = ['id', 'text', 'week']

class SharedContentSerializer(serializers.HyperlinkedModelSerializer):

    contentAuthor = AccountSerializer(many=False, help_text="Creator of content")  

    class Meta:
        model = SharedContent
        fields = ['id', 'contentText', 'contentAuthor', 'contentType', 'likeAmount']

    
class LikedContentSerializer(serializers.HyperlinkedModelSerializer):

    content = SharedContentSerializer(many=False, help_text="a single shared content instance")
    likeGiver = AccountSerializer(many=False, help_text='User who liked the shared content')

    class Meta:
        model = LikedContent
        fields = ['id', 'content', 'likeGiver']
from django.urls import reverse

from .bne_base import BneBaseTest
from ..models import Reward, Topic, Challenge, ChallengeProgress, ChallengeParticipation, UserChallenge, SelfCommitment


class RewardTestCase(BneBaseTest):

    def setUp(self):
        super().setUp()
        # Create Topics and Challenges
        tpc1 = Topic(internal_id=1, topic_name='Topic 1', image_level1='img_level1.png')
        tpc2 = Topic(internal_id=2, topic_name='Topic 2', image_level1='img_level2.png')
        selfcommitment, created = SelfCommitment.objects.get_or_create(self_commitment1="", self_commitment2="", self_commitment3="", self_commitment4="", self_commitment5="")
        self.supply_objects(tpc1, tpc2)

        clg1 = UserChallenge(title='Challenge 1', description='Desc 1', icon='icon1.png', image='img1.png', duration=1,
                         color='111111', difficulty=1, periodicity=1, topic=tpc1, selfCommitment=selfcommitment)
        clg2 = UserChallenge(title='Challenge 2', description='Desc 2', icon='icon2.png', image='img2.png', duration=2,
                         color='222222', difficulty=2, periodicity=1, topic=tpc2, selfCommitment=selfcommitment)
        self.supply_objects(clg1, clg2)

    def createRewards(self, cond):
        reward = Reward(type="MEDAL", color="#121212", points=10, condition=cond)
        reward1 = Reward(type="TROPHY", color="#121212", points=10, condition=cond)
        self.supply_objects(reward, reward1)

        return [reward.pk, reward1.pk]

    def createParticipation(self, title):
        clg = Challenge.objects.filter(title=title)[0]
        return self.client.post(reverse('challenge-participation', args=[clg.pk]), format='json')

    def createProgress(self, clg):
        return self.client.post(reverse('challenge-progress', args=[clg.pk]), format='json')

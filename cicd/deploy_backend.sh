#!/bin/bash

# causes the script to abort for any command that fails
set -e

CI_BUILD_TOKEN="{CI_BUILD_TOKEN}"
BACKEND_LATEST="{BACKEND_LATEST}"
DJANGO_ENV="{DJANGO_ENV}"

docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
docker pull $BACKEND_LATEST
docker container stop besmarth_backend && docker container rm besmarth_backend || true

docker run -d \
--volume besmarth_backend:/var/lib/besmarth \
--volume /etc/timezone:/etc/timezone:ro \
--volume /etc/localtime:/etc/localtime:ro \
--publish 80:80 \
--network besmarth \
--restart always \
--name besmarth_backend \
--env ENVIRONMENT=$DJANGO_ENV \
--expose 80 \
$BACKEND_LATEST